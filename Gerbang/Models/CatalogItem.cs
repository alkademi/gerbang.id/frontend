﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gerbang.Models
{
    public class CatalogItem
    {
        public string Id { get; set; }
        public string Logo { get; set; }
        public string Name { get; set; }
        public string Publisher  { get; set; }
        public string Rating { get; set; }
        public string Size { get; set; }
        public string Age { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
           
        public string RatingSize
        {
            get { return $"{Rating}   {Size}"; }
        }
    }
}
