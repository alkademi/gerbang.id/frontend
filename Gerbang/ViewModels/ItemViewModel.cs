﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Gerbang.Model.Product;
using Gerbang.Services;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Gerbang.ViewModels
{
    public class ItemViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        IProduct _rest = DependencyService.Get<IProduct>();

        public ItemViewModel()
        {
            getAllProductTypes();
            getAllProductCategories();

            AddLogoCommand = new Command(OnAddLogoCommand);
            AddImagesCommand = new Command(OnAddImagesCommand);
        }

        // Product
        private ObservableCollection<ProductModel> product;

        public ObservableCollection<ProductModel> Product
        {
            get { return product; }
            set { product = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Product"));
            }
        }


        // Product Types
        private ObservableCollection<ProductTypesModel> productTypes;

        public ObservableCollection<ProductTypesModel> ProductTypes
        {
            get { return productTypes; }
            set
            {
                productTypes = value;

                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ProductTypes"));
            }
        }

        // Product Categories
        private ObservableCollection<ProductCategory> productCategories;

        public ObservableCollection<ProductCategory> ProductCategories
        {
            get
            {
                if(SelectedType == null)
                {
                    return productCategories;
                }
                return new ObservableCollection<ProductCategory>(from item in productCategories where item.productTypesId == SelectedType.ProductTypesId select item);
            }
            set
            {
                productCategories = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ProductCategories"));
            }
        }

        public async Task<bool> postNewProduct()
        {
            ProductModel productModel = new ProductModel
            {
                productId = 0,
                productName = Name,
                productTypesId = SelectedType.ProductTypesId,
                productCategoryId = SelectedCategory.categoryId,
                productPublisher = Penerbit,
                productRating = Penilaian,
                productSize = Ukuran,
                productSizeUnit = Ukuran_Satuan,
                productAgeRestriction = Batasan_Umur,
                productDesc = Deskripsi,
                productLogo = Logo,
                productLogoName = Nama_Logo,
                productImages = Gambar_Pendukung,
                productImagesName = Nama_Gambar_Pendukung
            };

            return await _rest.postNewProduct(productModel);
        }

        public async Task<bool> updateProduct()
        {
            ProductModel productModel = new ProductModel
            {
                productId = Id,
                productName = Name,
                productTypesId = SelectedType.ProductTypesId,
                productCategoryId = SelectedCategory.categoryId,
                productPublisher = Penerbit,
                productRating = Penilaian,
                productSize = Ukuran,
                productSizeUnit = Ukuran_Satuan,
                productAgeRestriction = Batasan_Umur,
                productDesc = Deskripsi,
                productLogo = Logo,
                productLogoName = Nama_Logo,
                productImages = Gambar_Pendukung,
                productImagesName = Nama_Gambar_Pendukung
            };

            return await _rest.updateProduct(productModel);
        }

        public async void getAllProductTypes()
        {
            try
            {
                var result = await _rest.getProductTypes();
                if(result != null)
                {
                    ProductTypes = result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async void getAllProductCategories()
        {
            try
            {
                var result = await _rest.getProductCategories();
                if (result != null)
                {
                    ProductCategories = result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Command addLogoCommand;
        public Command AddLogoCommand
        {
            get
            {
                return addLogoCommand;
            }
            set
            {
                addLogoCommand = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AddLogoCommand"));
            }
        }

        private async void OnAddLogoCommand()
        {
            try
            {
                var file = await FilePicker.PickAsync(
                    new PickOptions
                    {
                        FileTypes = FilePickerFileType.Images
                    });

                if (file == null)
                {
                    return;
                }
                this.Nama_Logo = file.FileName;
                Console.WriteLine(Nama_Logo);
                var stream = await file.OpenReadAsync();
                byte[] filebytearray = new byte[stream.Length];
                stream.Read(filebytearray, 0, filebytearray.Length);
                this.Logo = $"data:{file.ContentType};base64,{Convert.ToBase64String(filebytearray)}";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Command addImagesCommand;
        public Command AddImagesCommand
        {
            get
            {
                return addImagesCommand;
            }
            set
            {
                addImagesCommand = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AddImagesCommand"));
            }
        }


        public string _add_supporting_images_text;
        public string Add_Supporting_Images_Text
        {
            get { return _add_supporting_images_text; }
            set
            {
                _add_supporting_images_text = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Add_Supporting_Images_Text"));
            }
        }

        private async void OnAddImagesCommand()
        {
            try
            {
                var files = await FilePicker.PickMultipleAsync(
                    new PickOptions
                    {
                        FileTypes = FilePickerFileType.Images
                    });

                if (files == null)
                {
                    return;
                }

                var files_title = "";

                List<string> supportingImages = new List<string>();
                List<string> supportingImagesName = new List<string>();
                string supportingImageName;

                using (var file = files.GetEnumerator())
                {
                    while (file.MoveNext())
                    {
                        supportingImageName = file.Current.FileName;
                        supportingImagesName.Add(supportingImageName);
                        this.Nama_Gambar_Pendukung = supportingImagesName;
                        files_title += supportingImageName + "\n";

                        var stream = await file.Current.OpenReadAsync();
                        byte[] filebytearray = new byte[stream.Length];
                        stream.Read(filebytearray, 0, filebytearray.Length);
                        supportingImages.Add($"data:{file.Current.ContentType};base64,{Convert.ToBase64String(filebytearray)}");
                        this.Gambar_Pendukung = supportingImages;
                    }
                }

                this.Add_Supporting_Images_Text = files_title;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public Command submitCommand;
        public Command SubmitCommand
        {
            get
            {
                return submitCommand;
            }
            set
            {
                submitCommand = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SubmitCommand"));
            }
        }

        /*                 DATA TO SEND            */
        // ID

        private int _id;

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Id"));
            }
        }

        // Name
        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
            }
        }

        // Selected Product Type
        private ProductTypesModel _selectedType;

        public ProductTypesModel SelectedType
        {
            get { return _selectedType; }
            set
            {
                _selectedType = value;
                ProductCategories = productCategories;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SelectedType"));
            }
        }

        // Selected Product Category
        private ProductCategory _selectedCategory;

        public ProductCategory SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                _selectedCategory = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SelectedCategory"));
            }
        }

        // Penerbit
        private string _penerbit;

        public string Penerbit
        {
            get { return _penerbit; }
            set
            {
                _penerbit = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Penerbit"));
            }
        }

        // Penilaian
        private float _penilaian;

        public float Penilaian
        {
            get { return _penilaian; }
            set
            {
                _penilaian = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Penilaian"));
            }
        }

        // Ukuran
        private float _ukuran;

        public float Ukuran
        {
            get { return _ukuran; }
            set
            {
                _ukuran = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Ukuran"));
            }
        }

        // Satuan dari Ukuran
        private int _ukuran_satuan;

        public string Ukuran_Satuan
        {
            get {
                switch (_ukuran_satuan)
                {
                    default:
                        return "";
                    case 0:
                        return "KB";
                    case 1:
                        return "MB";
                    case 2:
                        return "GB";
                }
            }
            set
            {
                var isNumeric = int.TryParse(value, out int n);
                if(isNumeric)
                {
                    _ukuran_satuan = n;
                } else
                {
                    switch (value)
                    {
                        case "KB":
                            _ukuran_satuan = 0;
                            break;
                        case "MB":
                            _ukuran_satuan = 1;
                            break;
                        case "GB":
                            _ukuran_satuan = 2;
                            break;
                    }
                }
                
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Ukuran_Satuan"));
            }
        }

        // Batasan Umur
        private int _batasan_umur;

        public string Batasan_Umur
        {
            get {
                switch (_batasan_umur)
                {
                    case 0:
                        return "Anak-anak";
                    case 1:
                        return "Remaja";
                    case 2:
                        return "Dewasa";
                    default:
                        return "";
                }
            }
            set
            {
                var isNumeric = int.TryParse(value, out int n);
                if (isNumeric)
                {
                    _batasan_umur = n;
                } else
                {
                    switch (value)
                    {
                        case "Anak-anak":
                            _batasan_umur = 0;
                            break;
                        case "Remaja":
                            _batasan_umur = 1;
                            break;
                        case "Dewasa":
                            _batasan_umur = 2;
                            break;
                    }
                }
            
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Batasan_Umur"));
            }
        }

        // Deskripsi
        private string _deskripsi;

        public string Deskripsi
        {
            get { return _deskripsi; }
            set
            {
                _deskripsi = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Deskripsi"));
            }
        }

        // Logo
        private string _logo;

        public string Logo
        {
            get { return _logo; }
            set
            {
                _logo = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Logo"));
            }
        }

        // Gambar Pendukung
        private List<string> _gambar_pendukung;

        public List<string> Gambar_Pendukung
        {
            get { return _gambar_pendukung; }
            set
            {
                _gambar_pendukung = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Gambar_Pendukung"));
            }
        }

        // Nama dari Logo
        private string _nama_logo;

        public string Nama_Logo
        {
            get { return _nama_logo; }
            set
            {
                _nama_logo = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Nama_Logo"));
            }
        }

        // Nama dari Gambar Pendukung
        private List<string> _nama_gambar_pendukung;

        public List<string> Nama_Gambar_Pendukung
        {
            get { return _nama_gambar_pendukung; }
            set
            {
                _nama_gambar_pendukung = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Nama_Gambar_Pendukung"));
            }
        }


        public async void getProduct(int id)
        {
            var result = await _rest.getSpecificProduct(id);
            if (result != null)
            {
                this.Name = result[0].productName;
                this.Penerbit = result[0].productPublisher;
                this.Penilaian = result[0].productRating;
                this.Ukuran = result[0].productSize;
                this.Ukuran_Satuan = result[0].productSizeUnit.ToString();
                this.Batasan_Umur = result[0].productAgeRestriction.ToString();
                this.Deskripsi = result[0].productDesc;
                this.Logo = result[0].productLogo;
                this.Gambar_Pendukung = result[0].productImages;
                this.Nama_Logo = result[0].productLogoName;
                this.Nama_Gambar_Pendukung = result[0].productImagesName;
                this.SelectedType = this.ProductTypes.Where(d => d.ProductTypesId == result[0].productTypesId).FirstOrDefault();
                this.SelectedCategory = this.ProductCategories.Where(d => d.categoryId == result[0].productCategoryId).FirstOrDefault();

                // show name of images
                var nama_gambar_tampil = "";
                foreach (var item in this.Nama_Gambar_Pendukung)
                {
                    nama_gambar_tampil += $"{item}\r\n";
                }

                this.Add_Supporting_Images_Text = nama_gambar_tampil;
            }
        }

    }
}
