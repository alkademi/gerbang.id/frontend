﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Gerbang.ViewModels
{
    public class ReviewItemViewModel : BaseViewModel
    {
        private int _reviewId;
        [JsonProperty("ReviewId")]
        public int ReviewId
        {
            get { return _reviewId; }
            set
            {
                _reviewId = value;
            }
        }
        private int _userId;
        [JsonProperty("UserId")]
        public int UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
            }
        }
        private int _productId;
        [JsonProperty("ProductId")]
        public int ProductId
        {
            get { return _productId; }
            set
            {
                _productId = value;
            }
        }
        private int _stars;
        [JsonProperty("Stars")]
        public int Stars
        {
            get { return _stars; }
            set
            {
                _stars = value;
            }
        }
        private string _reviewDate;
        [JsonProperty("ReviewDate")]
        public string ReviewDate
        {
            get { return _reviewDate; }
            set
            {
                _reviewDate = value;
            }
        }
        private string _reviewText;
        [JsonProperty("ReviewText")]
        public string ReviewText
        {
            get { return _reviewText; }
            set
            {
                _reviewText = value;
            }
        }
        private string _userName;
        [JsonProperty("UserName")]
        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
            }
        }

        public ReviewItemViewModel()
        {

           
        }
    }
}

