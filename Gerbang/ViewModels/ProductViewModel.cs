﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.IO;
using System;
using System.Threading.Tasks;
using Gerbang.Views;
using Xamarin.Essentials;

namespace Gerbang.ViewModels
{
    public class ProductViewModel : INotifyPropertyChanged
    {
        public IList<ProductItemViewModelWithId> source;
        ProductItemViewModelWithId selectedProduct;
        int selectionCount = 1;
        string content;

        private ObservableCollection<ProductItemViewModelWithId> _products { get; set; }
        public ObservableCollection<ProductItemViewModelWithId> Products
        {
            get { return _products; }
            set
            {
                if (_products != value)
                {
                    _products = value;
                    OnPropertyChanged();
                }
            }
        }

        

        private int _tab { get; set; }
        public int Tab
        {
            get { return _tab; }
            set
            {
                if (_tab != value)
                {
                    _tab = value;
                    OnPropertyChanged();
                }
            }
        }

        private int _type { get; set; }
        public int Type
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    _type = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool _isNotFound { get; set; }
        public bool IsNotFound
        {
            get { return _isNotFound; }
            set
            {
                if (_isNotFound != value)
                {
                    _isNotFound = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool _isLoading { get; set; }
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool _isLoaded { get; set; }
        public bool IsLoaded
        {
            get { return _isLoaded; }
            set
            {
                if (_isLoaded != value)
                {
                    _isLoaded = value;
                    OnPropertyChanged();
                }
            }
        }

        private List<CategoryViewModel> _categories { get; set; }
        public List<CategoryViewModel> Categories
        {
            get { return _categories; }
            set
            {
                if (_categories != value)
                {
                    _categories = value;
                    IsNotFound = false;
                    OnPropertyChanged();
                }
            }
        }

        public List<SortOptionViewModel> SortOptions { get; set; }
        public IList<ProductItemViewModelWithId> EmptyProducts { get; set; }

        INavigation Navigation => Application.Current.MainPage.Navigation;
        public ProductItemViewModelWithId SelectedProduct
        {
            get
            {
                return selectedProduct;
            }
            set
            {
                if (selectedProduct != value)
                {
                    selectedProduct = value;
                }
            }
        }

        ObservableCollection<object> selectedProducts;
        public ObservableCollection<object> SelectedProducts
        {
            get
            {
                return selectedProducts;
            }
            set
            {
                if (selectedProducts != value)
                {
                    selectedProducts = value;
                }
            }
        }

        public string SelectedProductMessage { get; set; }

        

        public ICommand DeleteCommand => new Command<ProductItemViewModelWithId>(RemoveProduct);
        //public ICommand FilterCommand => new Command<string>(FilterItems);
        public ICommand ProductSelectionCommand{
            get
            {
                return new Command(async () =>
                {
                    if (SelectedItem == null)
                    {
                        return;
                    }
                    else
                    {
                        //await Navigation.PushAsync(new DetailPageAdmin(SelectedItem, 1));
                        //Console.WriteLine("Masuk ke selection");
                        var isAdmin = App.Current.Properties ["Role"];
                        if (String.Equals(isAdmin, "Admin"))
                        {
                            //Console.WriteLine("masok detail admin");
                            await Navigation.PushAsync(new DetailPageAdmin(SelectedItem));
                        }
                        else
                        {
                            //Console.WriteLine("masok detail user");
                            await Navigation.PushAsync(new ItemDetailPage(SelectedItem));
                        }
                    }
                    SelectedItem = null;
                });
            }
        }
        public ICommand WishlistSelectionCommand
        {
            get
            {
                return new Command(async () =>
                {
                    if (SelectedItem == null)
                    {
                        return;
                    }
                    else
                    {
                        //await Navigation.PushAsync(new DetailPageAdmin(SelectedItem, 1));
                        var isAdmin = App.Current.Properties["Role"];
                        if (String.Equals(isAdmin, "Admin"))
                        {
                            //Console.WriteLine("masok detail admin");
                            await Navigation.PushAsync(new DetailPageAdmin(SelectedItem,1));
                        }
                        else
                        {
                            //Console.WriteLine("masok detail user");
                            await Navigation.PushAsync(new ItemDetailPage(SelectedItem,1));
                        }

                    }
                    SelectedItem = null;
                });
            }
        }

        public ProductItemViewModelWithId SelectedItem { get; set; }
        public ProductViewModel()
        {
            source = new ObservableCollection<ProductItemViewModelWithId>();
            SortOptions = GetSortOptions();
            Tab = 0;
            Type = 1;
            IsNotFound = false;
            IsLoading = true;
            IsLoaded = false;
            /*CreateProductCollection();*/


            //selectedProduct = Products.Skip(3).FirstOrDefault();
            //ProductSelectionChanged();

            //SelectedProducts = new ObservableCollection<object>()

        }
        //public async void fetchProduct()
        //{
        //    HttpClient cli = new HttpClient();
        //    var url =
        //    var content = await cli.GetStringAsync(url);

        //}
        

        public async void CreateProductCollection()
        {
            HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
            HttpClient cli = new HttpClient(handler);

            var url = "http://20.127.56.222:8080/api/Product";
            var content = await cli.GetStringAsync(url);
            Console.WriteLine(content[content.Length - 1]);
            /*       source.Add(new ProductItemViewModelWithId
                   {
                       ProductId = 1,
                       ProductLogo = "https://1.bp.blogspot.com/-zqp4iusg6UE/YAgr9PkZVKI/AAAAAAAADwk/IiXI8rQg11IFWIG2gcnV4ECqwn-feeQGQCLcBGAsYHQ/s1600/Logo%2BTwitter.png",
                       ProductName = "Twitter",
                       ProductPublisher = "Twitter",
                       ProductRating = 4.3F,
                       ProductSize = 69

                   });
                   source.Add(new ProductItemViewModelWithId
                   {
                       ProductId = 2,
                       ProductLogo = "https://1.bp.blogspot.com/-zqp4iusg6UE/YAgr9PkZVKI/AAAAAAAADwk/IiXI8rQg11IFWIG2gcnV4ECqwn-feeQGQCLcBGAsYHQ/s1600/Logo%2BTwitter.png",
                       ProductName = "Twitter",
                       ProductPublisher = "Twitter",
                       ProductRating = 4.3F,
                       ProductSize = 69

                   });*/
            Products = new ObservableCollection<ProductItemViewModelWithId>(JsonConvert.DeserializeObject<List<ProductItemViewModelWithId>>(content));
        }
        //void FilterItems(string filter)
        //{
        //    var filteredItems = source.Where(Product => Product.Name.ToLower().Contains(filter.ToLower())).ToList();
        //    foreach (var Product in source)
        //    {
        //        if (!filteredItems.Contains(Product))
        //        {
        //            Products.Remove(Product);
        //        }
        //        else
        //        {
        //            if (!Products.Contains(Product))
        //            {
        //                Products.Add(Product);
        //            }
        //        }
        //    }
        //}

        //void ProductSelectionChanged()
        //{
        //    SelectedProductMessage = $"Selection {selectionCount}: {SelectedProduct.Name}";
        //    OnPropertyChanged("SelectedProductMessage");
        //    selectionCount++;
        //}

        void RemoveProduct(ProductItemViewModelWithId Product)
        {
            if (Products.Contains(Product))
            {
                Products.Remove(Product);
            }
        }

        public List<SortOptionViewModel> GetSortOptions()
        {
            var options = new List<SortOptionViewModel>()
            {
                new SortOptionViewModel(){SortOptionId=1, SortOptionName="Nama (A-Z)"},
                new SortOptionViewModel(){SortOptionId=2, SortOptionName="Rating (Tertinggi)"},
                new SortOptionViewModel(){SortOptionId=3, SortOptionName="Rating (Terendah)"},
            };
            return options;
        }


        private CategoryViewModel _selectedCategory { get; set; }
        public CategoryViewModel SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                if(_selectedCategory != value)
                {
                    _selectedCategory = value;
                    GetProductsWithFilter();
                }
            }
        }

        private SortOptionViewModel _selectedSortOption { get; set; }
        public SortOptionViewModel SelectedSortOption
        {
            get { return _selectedSortOption; }
            set
            {
                if (_selectedSortOption != value)
                {
                    _selectedSortOption = value;
                    GetProductsWithFilter();
                }
            }
        }

        private async void GetProductsWithFilter()
        {
            IsNotFound = false;
            IsLoading = true;
            IsLoaded = false;
            HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
            HttpClient cli = new HttpClient(handler);
            ProductViewModel pvm = new ProductViewModel();
            var url = "http://20.127.56.222:8080/api/Product?" ;
            if (_selectedCategory != null)
            {
                url = url + $"&category={_selectedCategory.CategoryId}";
            }

            url = url + $"&type={Type}&tab={Tab}";

            if (_selectedSortOption != null)
            {
                if(_selectedSortOption.SortOptionId == 1)
                {
                    url = url + $"&sort_by=1&asc=true";
                }
                else if (_selectedSortOption.SortOptionId == 2)
                {
                    url = url + $"&sort_by=2&asc=false";
                }
                else if (_selectedSortOption.SortOptionId == 3)
                {
                    url = url + $"&sort_by=2&asc=true";
                }

            }

            var content = await cli.GetStringAsync(url);
            var ProductsWithoutId = new ObservableCollection<ProductItemViewModel>(JsonConvert.DeserializeObject<List<ProductItemViewModel>>(content));
            var filteredProducts = new ObservableCollection<ProductItemViewModelWithId>();
            if(ProductsWithoutId.Count > 0)
            {
                int i = 1;
                foreach (ProductItemViewModel item in ProductsWithoutId)
                {
                    filteredProducts.Add(new ProductItemViewModelWithId(i, item));
                    i++;
                }
            }
            if (filteredProducts.Count <= 0)
            {
                IsNotFound = true;
            }
            else
            {
                IsNotFound = false;
            }
            Products = filteredProducts;
            IsLoading = false;
            IsLoaded = true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
