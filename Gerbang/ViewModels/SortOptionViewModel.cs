﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Gerbang.ViewModels
{
    public class SortOptionViewModel : BaseViewModel
    {
        private int _sortOptionId;
        [JsonProperty("SortOptionId")]
        public int SortOptionId
        {
            get { return _sortOptionId; }
            set
            {
                _sortOptionId = value;
                //OnPropertyChanged();
            }
        }
        private string _sortOptionName;
        [JsonProperty("SortOptionName")]
        public string SortOptionName
        {
            get { return _sortOptionName; }
            set
            {
                _sortOptionName = value;
                //OnPropertyChanged();
            }
        }
    }
}
