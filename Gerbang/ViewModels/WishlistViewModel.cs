﻿using Gerbang.Models;
using Gerbang.Views;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Gerbang.ViewModels
{
    public class WishlistViewModel : BaseViewModel
    {
        private int _id_wishlist;
        [JsonProperty("id_wishlist")]
        public int IdWishlist
        {
            get { return _id_wishlist; }
            set
            {
                _id_wishlist = value;
            }
        }
        private int _id_product;
        [JsonProperty("id_product")]
        public int IdProduct
        {
            get { return _id_product; }
            set
            {
                _id_product = value;
            }
        }
        private string _username;
        [JsonProperty("username")]
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
            }
        }

        public WishlistViewModel()
        {
        }
    }
}