﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Gerbang.ViewModels
{
    public class CategoryViewModel : BaseViewModel
    {
        private int _categoryId;
        [JsonProperty("CategoryId")]
        public int CategoryId
        {
            get { return _categoryId; }
            set
            {
                _categoryId = value;
                //OnPropertyChanged();
            }
        }
        private string _categoryName;
        [JsonProperty("CategoryName")]
        public string CategoryName
        {
            get { return _categoryName; }
            set
            {
                _categoryName = value;
                //OnPropertyChanged();
            }
        }
    }
}
