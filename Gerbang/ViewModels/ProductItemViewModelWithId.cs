﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Gerbang.ViewModels
{
    public class ProductItemViewModelWithId : BaseViewModel
    {
        public enum size_units
        {
            kb,
            mb,
            gb
        }
        public enum ages
        {
            child,
            teen,
            adult
        }
        private int _index;
        public int Index
        {
            get { return _index; }
            set
            {
                _index = value;
                //OnPropertyChanged();
            }
        }
        private int _productId;
        [JsonProperty("ProductId")]
        public int ProductId
        {
            get { return _productId; }
            set
            {
                _productId = value;
                //OnPropertyChanged();
            }
        }
        private string _productName;
        [JsonProperty("ProductName")]
        public string ProductName
        {
            get { return _productName; }
            set
            {
                _productName = value;
                //OnPropertyChanged();
            }
        }
        private int _productTypesId;
        [JsonProperty("ProductTypesId")]
        public int ProductTypesId
        {
            get { return _productTypesId; }
            set
            {
                _productTypesId = value;
                //OnPropertyChanged();
            }
        }
        private int _productCategoryId;
        [JsonProperty("ProductCategoryId")]
        public int ProductCategoryId
        {
            get { return _productCategoryId; }
            set
            {
                _productCategoryId = value;
                //OnPropertyChanged();
            }
        }
        private string _productPublisher;
        [JsonProperty("ProductPublisher")]
        public string ProductPublisher
        {
            get { return _productPublisher; }
            set
            {
                _productPublisher = value;
                //OnPropertyChanged();
            }
        }
        private float _productRating;
        [JsonProperty("ProductRating")]
        public float ProductRating
        {
            get { return _productRating; }
            set
            {
                _productRating = value;
                //OnPropertyChanged();
            }
        }
        private float _productSize;
        [JsonProperty("ProductSize")]
        public float ProductSize
        {
            get { return _productSize; }
            set
            {
                _productSize = value;
                //OnPropertyChanged();
            }
        }
        private size_units _productSizeUnit;
        [JsonProperty("ProductSizeUnit")]
        public size_units ProductSizeUnit
        {
            get { return _productSizeUnit; }
            set
            {
                _productSizeUnit = value;
                //OnPropertyChanged();
            }
        }
        private ages _productAgeRestriction;
        [JsonProperty("ProductAgeRestriction")]
        public ages ProductAgeRestriction
        {
            get { return _productAgeRestriction; }
            set
            {
                _productAgeRestriction = value;
                //OnPropertyChanged();
            }
        }
        private string _productDesc;
        [JsonProperty("ProductDesc")]
        public string ProductDesc
        {
            get { return _productDesc; }
            set
            {
                _productDesc = value;
                //OnPropertyChanged();
            }
        }
        private string _productLogo;
        [JsonProperty("ProductLogo")]
        public string ProductLogo
        {
            get {

                return _productLogo;
            
            }
            set
            {
                _productLogo = value;
                //OnPropertyChanged();
            }
        }
        private string _productLogoName;
        [JsonProperty("ProductLogoName")]
        public string ProductLogoName
        {
            get { return _productLogoName; }
            set
            {
                _productLogoName = value;
                //OnPropertyChanged();
            }
        }
        private string[] _productImages;
        [JsonProperty("ProductImages")]
        public string[] ProductImages
        {
            get { return _productImages; }
            set
            {
                _productImages = value;
                //OnPropertyChanged();
            }
        }
        private string[] _productImagesName;
        [JsonProperty("ProductImagesName")]
        public string[] ProductImagesName
        {
            get { return _productImagesName; }
            set
            {
                _productImagesName = value;
                //OnPropertyChanged();
            }
        }

        public string RatingSize
        {
            get { return $"{ProductRating}★  {ProductSize} {ProductSizeUnit.ToString().ToUpper()}"; }
        }

        public ImageSource LogoSrc
        {
            get
            {
                try
                {
                    string tempstr = _productLogo.Split(',')[1];
                    Console.WriteLine(tempstr);
                    var byteVal = Convert.FromBase64String(tempstr);
                    Stream stream = new MemoryStream(byteVal);
                    var imageSource = ImageSource.FromStream(() => stream);
                    return imageSource;
                }
                catch (Exception ex)
                {
                    string tempstr2 = "iVBORw0KGgoAAAANSUhEUgAAAZAAAACWCAYAAADwkd5lAAAONUlEQVR4Xu2cO28USReGyw4MlpxgR0hEXAQ5IZcQ+AlABAkISLlIxCQgAiIgQmQgYoRASCSQ45xLCJFtCSEZE9i71WzPtofxVHd1VdepU89Io293u7vq1POe7rfO6fE3s7y8vLW5uWnm5+er79zcnOEDAQhAAAIQGCfw+/dvs76+Xn1nZ2fNzLdv37aWlpbM6upq9bWfxcXF6ouZkEAQgAAEyiZgTWOSP6ysrPwxkL17944I/fz5c3TywsJCZSR79uwxMzMzZVNk9RCAAAQKIbC1tWXW1tYqL7CeUBcV1hPqz/fv3/82kPpgmwEKYckyIQABCBRBoEsBMdVAmrR2KmFocRWRUywSAhBQTMD3+d7aQJrsujiUYuYsDQIQgEC2BEJ0mLwMhBZXtjlD4BCAQOEEQhYAvQyEFlfhmcjyIQCBLAj4tqhciwtmILS4XKg5DgEIQGA4AiFaVK5ooxgILS4Xdo5DAAIQiEMgZIvKFWFUA6HF5cLPcQhAAAL9CcRqUbkiG8xAaHG5pOA4BCAAgfYEhmhRuaJJYiC0uFyycBwCEIDAZAJDtqhcGiQ1EFpcLnk4DgEIQMCYVC0qF3sxBkKLyyUVxyEAgZIISGhRuXiLNBBaXC7ZOA4BCGglIKlF5WIs2kBocbnk4zgEIKCBgNQWlYttNgZCi8slJcchAIGcCOTQonLxzNJAaHG5ZOU4BCAglUBOLSoXw6wNhBaXS16OQwACEgjk2qJysVNjILS4XFJzHAIQGJKAhhaVi5dKA6HF5ZKd4xCAQCwCmlpULkaqDYQWl0t+jkMAAiEIaG1RudgUYyC0uFypwHEIQKALgRJaVC4eRRoILS5XWnAcAhDYiUBJLSpXFhRtILS4XOnBcQhAwBIotUXlUh8DmUCIHYYrbTgOAf0EaFG5NcZApjAigdwJxBkQ0EaADWR7RTGQlqwoYVuC4jQIZEiA+9tPNAzEgxs7FA9oXAIBYQToMPQXBAPpwZAE7AGPSyGQiAAbwHDgMZBALCmBA4FkGAhEIMD9GQHqv0NiIBG4ssOJAJUhIdCRAB2CjsA8TsdAPKC1vYQEbkuK8yAQjgAbuHAsXSNhIC5CgY5TQgcCyTAQmECA+ytNWmAgCbizQ0oAnSnVEaDCTy8pBpJQA26AhPCZOlsCbMDkSIeBCNGCElyIEIQhkgD3h0hZ+BWWRFnYYUlUhZiGJkCFPjTx7vNRgXRnNtgV3ECDoWYiQQTYQAkSwxEKBpKJVpTwmQhFmF4EyG8vbMkvwkCSS9A9AHZo3ZlxhTwCVNjyNOkaEQbSlZig87kBBYlBKK0JsAFqjUr8iRiIeInaBUgLoB0nzkpDgPxMwz32rBhIbMIJxmeHlwA6U/5FgApZf1JgIIo15gZWLK7gpbGBESxO4NAwkMBApQ5HC0GqMjriIr906Nh1FRhIV2IKzmeHqEBEAUugwhUgQuIQMJDEAqScngdASvr5zs0GJF/tQkeOgYQmmul4tCAyFW6gsMmPgUBnNg0GkplgQ4TLDnMIyvLnoEKVr1HqCDGQ1AoInp8HiGBxIobGBiIiXGVDYyDKBI21HFoYscjKGBd9ZeiQWxQYSG6KCYiXHaoAEQKEQIUZAGLhQ2AghSdAn+XzAOpDL921bADSsdc2MwaiTdFE66EFkgh8y2nRpyUoTutEAAPphIuT2xBgh9uGUvxzqBDjMy59Bgyk9AyIuH4eYBHhThkaA0/DvcRZMZASVU+wZloocaHDNy5fRp9MAAMhMwYnwA45DHIqvDAcGcWfAAbiz44rexLgAegHEAP248ZV4QlgIOGZMqIHAVow06HBxyOpuCQ6AQwkOmIm6EqAHfYfYlRoXTOH84cmgIEMTZz5WhMo9QGKgbZOEU5MTAADSSwA07cjoL2Fo3197VTmrNwIYCC5KUa8RssOvdQKixTWQwAD0aNlcSvJ9QGsxQCLSzgW/BcBDISkUEFAegtIenwqkoBFDE4AAxkcORPGJiBlh59rhRRbH8bXQwAD0aMlKxkjkOoBLsXASAgIxCaAgcQmzPgiCMRuIcUeXwREgoDAGAEMhJQojkCoCiFVhVOcYCxYLAEMRKw0BBabgK8BhDKg2OtjfAjEJoCBxCbM+FkQcLWgXMezWCRBQiAwAQwkMFCGy59AXWGsrKyY3bt3Vwva2Ngwi4uL1XdhYSH/RbICCAQggIEEgMgQugg0W1S7du2qFvfr1y+ztLSEgeiSmtX0JICB9ATI5ToIuFpUruM6KLAKCHQjgIF048XZigjwEl2RmCwlCQEMJAl2Jk1JINSvqHwNKOXamRsCIQlgICFpMpZYArFbULHHFwuWwIomgIEULb/uxaeqEEJVOLrVYXUaCGAgGlRkDdsISHmApzIw0gECQxHAQIYizTxRCUhvIUmPL6o4DK6WAAaiVlr9C8t1hy+lQtKfIawwNgEMJDZhxg9OQMsDOFcDDC4oA2ZLAAPJVrqyAtfeAtK+vrKytZzVYiDlaJ3dSkvdoWupsLJLOALuTAAD6YyMC2IT4AH6h3CpBho7vxg/HAEMJBxLRupBgBbOdHjw6ZFcXBqNAAYSDS0Duwiww3YRmnycCs2PG1eFJ4CBhGfKiA4CPADDpAgGHIYjo/gTwED82XFlBwK0YDrA8jgVvh7QuKQ3AQykN0IG2IkAO+Q0uUGFl4Z7ibNiICWqHnnNPMAiA245PAbeEhSneRPAQLzRcWGTAC0U2fmAPrL1yTU6DCRX5QTEzQ5XgAgeIVAhekDjkokEMBASozMBHkCdkYm8gA2ASFmyCgoDyUqudMHSAknHfoiZ0XcIyvrmwED0aRpsRexQg6HMaiAqzKzkShosBpIUv8zJeYDI1GXoqNhADE08v/kwkPw0ixIxLYwoWNUMSn6okTLoQjCQoDjzGowdZl56SYmWClWKEunjwEDSazB4BDwABkeuckI2ICpl7bQoDKQTrnxPpgWRr3Y5RE5+5aBS+BgxkPBMxYzIDlGMFEUFQoVbjtwYiEKtuYEViprhktjAZChax5AxkI7ApJ5OC0GqMsRlCZCfOvMAA8lYV3Z4GYtXcOhUyHrEx0Ay1JIbMEPRCPkvAmyA8k8KDCQTDWkBZCIUYXoRIL+9sCW/CANJLsHOAbBDEywOoUUjQIUdDW3wgTGQ4Ej7D8gN1J8hI+RPgA2UfA0xECEaUcILEYIwRBLg/hApi8FAEurCDishfKbOlgAVuhzpMJAEWnADJIDOlOoIsAFLLykGMpAGlOADgWaaIglwf6WRHQOJyJ0dUkS4DA2BHQhQ4Q+XGhhIBNYkcASoDAmBjgTYwHUE5nE6BuIBbdIllNCBQDIMBCIQ4P6MAPXfITGQHlzZ4fSAx6UQSESADkE48BiIB0sS0AMal0BAGAE2gP0FwUBaMqQEbgmK0yCQIQHubz/RMJAp3Nih+CUVV0EgZwJ0GNqrh4FMYEUCtU8gzoSAVgJsIN3KYiD/MaKEdScLZ0CgVAI8HyYrX7SBsMMo9XHAuiHgT4AOxf/sijQQEsD/5uFKCEDgDwE2oAX9HQglKLc9BCAQi0CpzxfVFQg7hFi3C+NCAAI7ESipw6HSQEoSkNsYAhCQSaCEDawaAym1hJR56xAVBCDQJKD1+ZS1gZTg8NyGEICALgKaOiRZGogmAXTdGqwGAhBoS0DDBjgbA9FaArZNNs6DAAT0Esj1+SbaQDQ4tN6UZ2UQgEAMAjl1WEQaSE4AYyQQY0IAAhDIYQMtxkByLeFIcwhAAAKxCUh9PiY1kBwcNnZiMD4EIACBLgQkdWiSGIgkAF2E41wIQAACMQicOXPG7N+/3zx8+HA0/MzMzOif7Wa7+Wke+/Tpk7HP1MXFxeq7sLAwNcSvX7+aAwcOVP9fXvXn+fPn5ty5c9uu+/LlSxWT/ewUy2AGIrUEi5EMjAkBCECgLYEPHz6YEydOmCtXrowM5OrVq9Xl1lCa/2z/26RjDx48MKurq9XXfmozmZub2xZGbR72PzYN5N69e+bdu3fm9evXf4U9LZaoBkKLqm0KcR4EIFAqgYMHD1ZLP3Xq1MhA7I7//fv35vjx46Y2mPqBP+2YHWenDs+1a9fMo0ePzOnTp82bN2+2Gci4STW1mDZfFAOhRVXqrcC6IQCBLgTszt9+7O6/bmHVVULdQmr+uz3Xtp8mHbt//35lELXRPHv2zJw/f97Y/z1y5Ih58eKFuX37tnn58mXVrmpWILaFduHCBXP27NmJFcuk+Wy8wQyEFlWXtOFcCEAAAsbY6uPz58+m+Q6krjjGH9q2IrEf2+6adMxWK/U4169fr4zGmoc1hebz+dWrV+bGjRtmY2PD1C0uW2XUxmTnuHv3rrl58+ao+tlpvl4GQouKWwACEICAHwHbNjp58mT1gA9lIDaS+oV3851KM8KnT5+aixcvmo8fP1Yv3H/8+GGOHj06MpvawKz57Nu3b6pheRkILSq/hOEqCEAAApaAfUjfuXNn9NK6aSC+Laz6F1O2LXbr1q1tLaom9foXV5ubm2Ztba168T7+K67xSqZ3C4sWFYkPAQhAIAyB+iE/Ppp9wW1/CeX7Er02H9uOsu2xSb+qqg2k+Q5k/PluX7gfPnzYPH78eGosUysQWlRhkoVRIAABCEwjMP53IM1/H/+F1LRj1jQuXbpUvb+wJlS/A5lUgdQGMv7O5e3bt9Uvwp48eVK1ry5fvlyZyaSfFE80EFpUJDsEIACB4Qj0+UPC2gjqnwPbl/L2U1ca9c+B69VMqkDG/5DQXnPs2LFRi+vQoUMjGM3KZWQgS0tLrf4QZTikzAQBCEAAAhII7PQKY2VlxcwsLy9v2Rcq8/Pz1Xf8rxclLIAYIAABCEAgPQFrJuvr69V3dnbW/AOAogzIinOWsAAAAABJRU5ErkJggg==";
                    var byteVal2 = Convert.FromBase64String(tempstr2);
                    Stream stream2 = new MemoryStream(byteVal2);
                    var imageSource2 = ImageSource.FromStream(() => stream2);
                    return imageSource2;
                }
            }
        }

        public ImageSource[] ImageSrc
        {
            get
            {
                
                try
                {
                    ImageSource[] imgSrc = new ImageSource[_productImages.Length];
                    
                    for (int i=0;  i < _productImages.Length; i++) 
                    {
                        string imgconv = _productImages[i].Split(',')[1];
                        var byteVal = Convert.FromBase64String(imgconv);
                        Stream stream = new MemoryStream(byteVal);
                        var imageSource = ImageSource.FromStream(() => stream);
                        imgSrc[i] = imageSource;
                    } return imgSrc;
                } catch (Exception ex)
                {
                    ImageSource[] imgSrc2 = new ImageSource[_productImages.Length];
                    for (int j = 0; j < _productImages.Length; j++)
                    {
                        string imgconv2 = "iVBORw0KGgoAAAANSUhEUgAAAZAAAACWCAYAAADwkd5lAAAONUlEQVR4Xu2cO28USReGyw4MlpxgR0hEXAQ5IZcQ+AlABAkISLlIxCQgAiIgQmQgYoRASCSQ45xLCJFtCSEZE9i71WzPtofxVHd1VdepU89Io293u7vq1POe7rfO6fE3s7y8vLW5uWnm5+er79zcnOEDAQhAAAIQGCfw+/dvs76+Xn1nZ2fNzLdv37aWlpbM6upq9bWfxcXF6ouZkEAQgAAEyiZgTWOSP6ysrPwxkL17944I/fz5c3TywsJCZSR79uwxMzMzZVNk9RCAAAQKIbC1tWXW1tYqL7CeUBcV1hPqz/fv3/82kPpgmwEKYckyIQABCBRBoEsBMdVAmrR2KmFocRWRUywSAhBQTMD3+d7aQJrsujiUYuYsDQIQgEC2BEJ0mLwMhBZXtjlD4BCAQOEEQhYAvQyEFlfhmcjyIQCBLAj4tqhciwtmILS4XKg5DgEIQGA4AiFaVK5ooxgILS4Xdo5DAAIQiEMgZIvKFWFUA6HF5cLPcQhAAAL9CcRqUbkiG8xAaHG5pOA4BCAAgfYEhmhRuaJJYiC0uFyycBwCEIDAZAJDtqhcGiQ1EFpcLnk4DgEIQMCYVC0qF3sxBkKLyyUVxyEAgZIISGhRuXiLNBBaXC7ZOA4BCGglIKlF5WIs2kBocbnk4zgEIKCBgNQWlYttNgZCi8slJcchAIGcCOTQonLxzNJAaHG5ZOU4BCAglUBOLSoXw6wNhBaXS16OQwACEgjk2qJysVNjILS4XFJzHAIQGJKAhhaVi5dKA6HF5ZKd4xCAQCwCmlpULkaqDYQWl0t+jkMAAiEIaG1RudgUYyC0uFypwHEIQKALgRJaVC4eRRoILS5XWnAcAhDYiUBJLSpXFhRtILS4XOnBcQhAwBIotUXlUh8DmUCIHYYrbTgOAf0EaFG5NcZApjAigdwJxBkQ0EaADWR7RTGQlqwoYVuC4jQIZEiA+9tPNAzEgxs7FA9oXAIBYQToMPQXBAPpwZAE7AGPSyGQiAAbwHDgMZBALCmBA4FkGAhEIMD9GQHqv0NiIBG4ssOJAJUhIdCRAB2CjsA8TsdAPKC1vYQEbkuK8yAQjgAbuHAsXSNhIC5CgY5TQgcCyTAQmECA+ytNWmAgCbizQ0oAnSnVEaDCTy8pBpJQA26AhPCZOlsCbMDkSIeBCNGCElyIEIQhkgD3h0hZ+BWWRFnYYUlUhZiGJkCFPjTx7vNRgXRnNtgV3ECDoWYiQQTYQAkSwxEKBpKJVpTwmQhFmF4EyG8vbMkvwkCSS9A9AHZo3ZlxhTwCVNjyNOkaEQbSlZig87kBBYlBKK0JsAFqjUr8iRiIeInaBUgLoB0nzkpDgPxMwz32rBhIbMIJxmeHlwA6U/5FgApZf1JgIIo15gZWLK7gpbGBESxO4NAwkMBApQ5HC0GqMjriIr906Nh1FRhIV2IKzmeHqEBEAUugwhUgQuIQMJDEAqScngdASvr5zs0GJF/tQkeOgYQmmul4tCAyFW6gsMmPgUBnNg0GkplgQ4TLDnMIyvLnoEKVr1HqCDGQ1AoInp8HiGBxIobGBiIiXGVDYyDKBI21HFoYscjKGBd9ZeiQWxQYSG6KCYiXHaoAEQKEQIUZAGLhQ2AghSdAn+XzAOpDL921bADSsdc2MwaiTdFE66EFkgh8y2nRpyUoTutEAAPphIuT2xBgh9uGUvxzqBDjMy59Bgyk9AyIuH4eYBHhThkaA0/DvcRZMZASVU+wZloocaHDNy5fRp9MAAMhMwYnwA45DHIqvDAcGcWfAAbiz44rexLgAegHEAP248ZV4QlgIOGZMqIHAVow06HBxyOpuCQ6AQwkOmIm6EqAHfYfYlRoXTOH84cmgIEMTZz5WhMo9QGKgbZOEU5MTAADSSwA07cjoL2Fo3197VTmrNwIYCC5KUa8RssOvdQKixTWQwAD0aNlcSvJ9QGsxQCLSzgW/BcBDISkUEFAegtIenwqkoBFDE4AAxkcORPGJiBlh59rhRRbH8bXQwAD0aMlKxkjkOoBLsXASAgIxCaAgcQmzPgiCMRuIcUeXwREgoDAGAEMhJQojkCoCiFVhVOcYCxYLAEMRKw0BBabgK8BhDKg2OtjfAjEJoCBxCbM+FkQcLWgXMezWCRBQiAwAQwkMFCGy59AXWGsrKyY3bt3Vwva2Ngwi4uL1XdhYSH/RbICCAQggIEEgMgQugg0W1S7du2qFvfr1y+ztLSEgeiSmtX0JICB9ATI5ToIuFpUruM6KLAKCHQjgIF048XZigjwEl2RmCwlCQEMJAl2Jk1JINSvqHwNKOXamRsCIQlgICFpMpZYArFbULHHFwuWwIomgIEULb/uxaeqEEJVOLrVYXUaCGAgGlRkDdsISHmApzIw0gECQxHAQIYizTxRCUhvIUmPL6o4DK6WAAaiVlr9C8t1hy+lQtKfIawwNgEMJDZhxg9OQMsDOFcDDC4oA2ZLAAPJVrqyAtfeAtK+vrKytZzVYiDlaJ3dSkvdoWupsLJLOALuTAAD6YyMC2IT4AH6h3CpBho7vxg/HAEMJBxLRupBgBbOdHjw6ZFcXBqNAAYSDS0Duwiww3YRmnycCs2PG1eFJ4CBhGfKiA4CPADDpAgGHIYjo/gTwED82XFlBwK0YDrA8jgVvh7QuKQ3AQykN0IG2IkAO+Q0uUGFl4Z7ibNiICWqHnnNPMAiA245PAbeEhSneRPAQLzRcWGTAC0U2fmAPrL1yTU6DCRX5QTEzQ5XgAgeIVAhekDjkokEMBASozMBHkCdkYm8gA2ASFmyCgoDyUqudMHSAknHfoiZ0XcIyvrmwED0aRpsRexQg6HMaiAqzKzkShosBpIUv8zJeYDI1GXoqNhADE08v/kwkPw0ixIxLYwoWNUMSn6okTLoQjCQoDjzGowdZl56SYmWClWKEunjwEDSazB4BDwABkeuckI2ICpl7bQoDKQTrnxPpgWRr3Y5RE5+5aBS+BgxkPBMxYzIDlGMFEUFQoVbjtwYiEKtuYEViprhktjAZChax5AxkI7ApJ5OC0GqMsRlCZCfOvMAA8lYV3Z4GYtXcOhUyHrEx0Ay1JIbMEPRCPkvAmyA8k8KDCQTDWkBZCIUYXoRIL+9sCW/CANJLsHOAbBDEywOoUUjQIUdDW3wgTGQ4Ej7D8gN1J8hI+RPgA2UfA0xECEaUcILEYIwRBLg/hApi8FAEurCDishfKbOlgAVuhzpMJAEWnADJIDOlOoIsAFLLykGMpAGlOADgWaaIglwf6WRHQOJyJ0dUkS4DA2BHQhQ4Q+XGhhIBNYkcASoDAmBjgTYwHUE5nE6BuIBbdIllNCBQDIMBCIQ4P6MAPXfITGQHlzZ4fSAx6UQSESADkE48BiIB0sS0AMal0BAGAE2gP0FwUBaMqQEbgmK0yCQIQHubz/RMJAp3Nih+CUVV0EgZwJ0GNqrh4FMYEUCtU8gzoSAVgJsIN3KYiD/MaKEdScLZ0CgVAI8HyYrX7SBsMMo9XHAuiHgT4AOxf/sijQQEsD/5uFKCEDgDwE2oAX9HQglKLc9BCAQi0CpzxfVFQg7hFi3C+NCAAI7ESipw6HSQEoSkNsYAhCQSaCEDawaAym1hJR56xAVBCDQJKD1+ZS1gZTg8NyGEICALgKaOiRZGogmAXTdGqwGAhBoS0DDBjgbA9FaArZNNs6DAAT0Esj1+SbaQDQ4tN6UZ2UQgEAMAjl1WEQaSE4AYyQQY0IAAhDIYQMtxkByLeFIcwhAAAKxCUh9PiY1kBwcNnZiMD4EIACBLgQkdWiSGIgkAF2E41wIQAACMQicOXPG7N+/3zx8+HA0/MzMzOif7Wa7+Wke+/Tpk7HP1MXFxeq7sLAwNcSvX7+aAwcOVP9fXvXn+fPn5ty5c9uu+/LlSxWT/ewUy2AGIrUEi5EMjAkBCECgLYEPHz6YEydOmCtXrowM5OrVq9Xl1lCa/2z/26RjDx48MKurq9XXfmozmZub2xZGbR72PzYN5N69e+bdu3fm9evXf4U9LZaoBkKLqm0KcR4EIFAqgYMHD1ZLP3Xq1MhA7I7//fv35vjx46Y2mPqBP+2YHWenDs+1a9fMo0ePzOnTp82bN2+2Gci4STW1mDZfFAOhRVXqrcC6IQCBLgTszt9+7O6/bmHVVULdQmr+uz3Xtp8mHbt//35lELXRPHv2zJw/f97Y/z1y5Ih58eKFuX37tnn58mXVrmpWILaFduHCBXP27NmJFcuk+Wy8wQyEFlWXtOFcCEAAAsbY6uPz58+m+Q6krjjGH9q2IrEf2+6adMxWK/U4169fr4zGmoc1hebz+dWrV+bGjRtmY2PD1C0uW2XUxmTnuHv3rrl58+ao+tlpvl4GQouKWwACEICAHwHbNjp58mT1gA9lIDaS+oV3851KM8KnT5+aixcvmo8fP1Yv3H/8+GGOHj06MpvawKz57Nu3b6pheRkILSq/hOEqCEAAApaAfUjfuXNn9NK6aSC+Laz6F1O2LXbr1q1tLaom9foXV5ubm2Ztba168T7+K67xSqZ3C4sWFYkPAQhAIAyB+iE/Ppp9wW1/CeX7Er02H9uOsu2xSb+qqg2k+Q5k/PluX7gfPnzYPH78eGosUysQWlRhkoVRIAABCEwjMP53IM1/H/+F1LRj1jQuXbpUvb+wJlS/A5lUgdQGMv7O5e3bt9Uvwp48eVK1ry5fvlyZyaSfFE80EFpUJDsEIACB4Qj0+UPC2gjqnwPbl/L2U1ca9c+B69VMqkDG/5DQXnPs2LFRi+vQoUMjGM3KZWQgS0tLrf4QZTikzAQBCEAAAhII7PQKY2VlxcwsLy9v2Rcq8/Pz1Xf8rxclLIAYIAABCEAgPQFrJuvr69V3dnbW/AOAogzIinOWsAAAAABJRU5ErkJggg==";
                        var byteVal2 = Convert.FromBase64String(imgconv2);
                        Stream stream2 = new MemoryStream(byteVal2);
                        var imageSource2 = ImageSource.FromStream(() => stream2);
                        imgSrc2[j] = imageSource2;
                    } return imgSrc2;
                }
            }
        }

        public ProductItemViewModelWithId(int idx, ProductItemViewModel product)
        {
            _index = idx;
            _productId = product.ProductId;
            _productName = product.ProductName;
            _productTypesId = product.ProductTypesId;
            _productCategoryId = product.ProductCategoryId;
            _productPublisher = product.ProductPublisher;
            _productRating = product.ProductRating;
            _productSize = product.ProductSize;
            _productSizeUnit = (size_units)product.ProductSizeUnit;
            _productAgeRestriction = (ages)product.ProductAgeRestriction;
            _productDesc = product.ProductDesc;
            _productLogo = product.ProductLogo;
            _productLogoName = product.ProductLogoName;
            _productImages = product.ProductImages;
            _productImagesName = product.ProductImagesName;
        }
    }
}
