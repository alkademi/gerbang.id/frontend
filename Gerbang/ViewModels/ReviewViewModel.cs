﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace Gerbang.ViewModels
{
    public class ReviewViewModel : INotifyPropertyChanged
    {

        private ObservableCollection<ReviewItemViewModel> _reviews { get; set; }
        public ObservableCollection<ReviewItemViewModel> Reviews
        {
            get { return _reviews; }
            set
            {
                if (_reviews != value)
                {
                    _reviews = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool _isUlasan { get; set; }
        public bool IsUlasan
        {
            get { return _isUlasan; }
            set
            {
                if (_isUlasan != value)
                {
                    _isUlasan = value;
                    OnPropertyChanged();
                }
            }
        }
        public ReviewViewModel()
        {
            
        }
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

}