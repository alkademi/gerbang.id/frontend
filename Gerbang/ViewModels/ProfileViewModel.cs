﻿using Gerbang.Models;
using Gerbang.Views;
using Gerbang.Services;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.ComponentModel;
namespace Gerbang.ViewModels
{
    public class ProfileViewModel: BaseViewModel
    {
        private string _name;
        [JsonProperty("fullName")]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                //OnPropertyChanged();
            }
        }
        private string _dob;
        [JsonProperty("dateOfBirth")]
        public string DateOfBirth
        {
            get { return _dob; }
            set
            {
                _dob = value;
                //OnPropertyChanged();
            }
        }
        private string _email;
        [JsonProperty("email")]
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                //OnPropertyChanged();
            }
        }
        private string _username;
        [JsonProperty("userName")]
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                //OnPropertyChanged();
            }
        }

        public ProfileViewModel()
        {
            
        }
    }

}