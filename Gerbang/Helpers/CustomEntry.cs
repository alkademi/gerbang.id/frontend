﻿using System;
using Gerbang.Helpers;
using Xamarin.Forms;

namespace Gerbang.Helpers
{
    public class CustomEntry : Entry
    {
        public static readonly BindableProperty CornerRadiusProperty =
            BindableProperty.Create("CornerRadius",typeof(int),typeof(CustomEntry),0);

        public int EntryCornerRadius
        {
            get => (int)GetValue(CornerRadiusProperty);
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly BindableProperty BorderColorPropery =
            BindableProperty.Create("BorderThickness", typeof(Color), typeof(CustomEntry), Color.AliceBlue);

        public Color EntryBorderColor
        {
            get => (Color)GetValue(BorderColorPropery);
            set { SetValue(BorderColorPropery, value); }
        }

        public static readonly BindableProperty IsPasswordFlagProperty =
        BindableProperty.Create("IsPasswordFlag", typeof(bool), typeof(CustomEntry), defaultBindingMode: BindingMode.OneWay);
        public bool IsPasswordFlag
        {
            get { return (bool)GetValue(IsPasswordFlagProperty); }
            set { SetValue(IsPasswordFlagProperty, value); }
        }
    }
}
