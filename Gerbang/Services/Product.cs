﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Gerbang.Model.Product;
using Newtonsoft.Json;

namespace Gerbang.Services
{
    public class Product : IProduct
    {
        string Base_url = "http://20.127.56.222:8080";

        private HttpClient client;

        public Product()
        {
            HttpClientHandler handler = ApiServices.GetInsecureHandler();
            client = new HttpClient(handler);
            client.BaseAddress = new Uri(Base_url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<ObservableCollection<ProductModel>> getAllProduct()
        {
            var response = await client.GetAsync("/api/Product" );
            if(response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                response.EnsureSuccessStatusCode();
                var responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<ObservableCollection<ProductModel>>(responseContent);
            }

            return null;
        }

        public async Task<ObservableCollection<ProductModel>> getSpecificProduct(int id)
        {
            var response = await client.GetAsync("/api/Product/" + id);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                response.EnsureSuccessStatusCode();
                var responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<ObservableCollection<ProductModel>>(responseContent);
            }

            return null;
        }

        public async Task<bool> postNewProduct(ProductModel productModel)
        {
            var content = new StringContent(JsonConvert.SerializeObject(productModel), Encoding.UTF8, "application/json");
            var response = await client.PostAsync("/api/Product", content);
            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> updateProduct(ProductModel productModel)
        {
            var content = new StringContent(JsonConvert.SerializeObject(productModel), Encoding.UTF8, "application/json");
            var response = await client.PutAsync("/api/Product", content);
            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<ObservableCollection<ProductTypesModel>> getProductTypes()
        {
            var response = await client.GetAsync("/api/ProductTypes");
            response.EnsureSuccessStatusCode();
            var responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<ObservableCollection<ProductTypesModel>>(responseContent);
        }

        public async Task<ObservableCollection<ProductCategory>> getProductCategories()
        {
            var response = await client.GetAsync("/api/Category");
            var responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<ObservableCollection<ProductCategory>>(responseContent);
        }

        public async Task<ObservableCollection<Review>> getReviewbyProduct(int id)
        {
            var response = await client.GetAsync("/api/ProductReviews?product_id=" + id);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                response.EnsureSuccessStatusCode();
                var responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<ObservableCollection<Review>>(responseContent);
            }

            return null;
        }
    }
}
