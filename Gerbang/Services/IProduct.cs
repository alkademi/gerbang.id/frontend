﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Gerbang.Model.Product;

namespace Gerbang.Services
{
    public interface IProduct
    {
        Task<ObservableCollection<ProductModel>> getAllProduct();
        Task<bool> postNewProduct(ProductModel productModel);
        Task<bool> updateProduct(ProductModel productModel);
        Task<ObservableCollection<ProductTypesModel>> getProductTypes();
        Task<ObservableCollection<ProductCategory>> getProductCategories();
        Task<ObservableCollection<ProductModel>> getSpecificProduct(int id);
        Task<ObservableCollection<Review>> getReviewbyProduct(int id);
    }
}
