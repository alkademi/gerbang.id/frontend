﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Gerbang.Model.Login;
using Gerbang.Model.Product;
using Gerbang.Models;
using Gerbang.ViewModels;
using Newtonsoft.Json;
using Xamarin.Essentials;

namespace Gerbang.Services
{
    public class ApiServices
    {

        private static ApiServices _ServiceClientInstance;
        public static ApiServices ServiceClientInstance
        {
            get
            {
                if (_ServiceClientInstance == null)
                    _ServiceClientInstance = new ApiServices();
                return _ServiceClientInstance;
            }
        }

        private JsonSerializer _serializer = new JsonSerializer();
        private HttpClient client;


        public ApiServices()
        {
            HttpClientHandler handler = GetInsecureHandler();
            client = new HttpClient(handler);
            client.BaseAddress = new Uri("http://20.127.56.222:8080/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<LoginApiResponseModel> AuthenticateUserAsync(string userUsername, string userPassword)
        {
            try
            {
                LoginApiRequestModel loginRequestModel = new LoginApiRequestModel()
                {
                    username = userUsername,
                    password = userPassword
                };
                var content = new StringContent(JsonConvert.SerializeObject(loginRequestModel), Encoding.UTF8, "application/json");
                var response = await client.PostAsync("api/Users/authenticate", content);
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    LoginApiResponseModel jsoncontent = _serializer.Deserialize<LoginApiResponseModel>(json);
                    await SecureStorage.SetAsync("token", jsoncontent.token);
                    await SecureStorage.SetAsync("username", jsoncontent.username);
                    return jsoncontent;
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<RoleUserResponse>> GetUserRole(string username)
        {
            List<RoleUserResponse> roles = new List<RoleUserResponse>();
            var url = "http://20.127.56.222:8080/api/Users/role?user=" + username;
            var response = await client.GetStringAsync(url);
            Console.WriteLine(response);
            if (response != null)
            {
                roles = JsonConvert.DeserializeObject<List<RoleUserResponse>>(response);
            }
            return roles;
        }
        

        public async Task<bool> RegisterUserAsync(string fullname, string dob, string remail, string rusername, string rpassword, string passwordCf)
        {
            bool Response = false;
            await Task.Run(() =>
            {
                HttpClientHandler handler = GetInsecureHandler();
                var client = new HttpClient(handler);

                var model = new Model.Register.RegisterBindingModel
                {
                    fullName = fullname,
                    dateOfBirth = dob,
                    email = remail,
                    username = rusername,
                    password = rpassword,
                    passwordConfirmation = passwordCf
                };

                var json = JsonConvert.SerializeObject(model);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = client.PostAsync("http://20.127.56.222:8080/api/Users", httpContent);
                var mystring = response.GetAwaiter().GetResult();

                if (response.Result.IsSuccessStatusCode)
                {
                    Response = true;
                }


            });
            return Response;
        }

        public async Task<bool> DeleteCatalogAsync(int id)
        {
            bool Response = false;
            await Task.Run(() =>
            {
                HttpClientHandler handler = GetInsecureHandler();
                var client = new HttpClient(handler);

               
                var response = client.DeleteAsync("http://20.127.56.222:8080/api/Product/" + id);
                var mystring = response.GetAwaiter().GetResult();

                if (response.Result.IsSuccessStatusCode)
                {
                    Response = true;
                }
            });
            return Response;
        }

        public async Task<bool> AddWishlistAsync(int id_product, string username)
        {
            HttpClientHandler handler = GetInsecureHandler();
            var client = new HttpClient(handler);
            var content = await client.GetStringAsync("http://20.127.56.222:8080/api/Wishlist");
            var wishlist = new ObservableCollection<WishlistViewModel>(JsonConvert.DeserializeObject<List<WishlistViewModel>>(content));
            var id_wishlist = wishlist.Count + 1;

            bool Response = false;

            foreach (var wish in wishlist)
            {
                if (wish.Username == username && wish.IdProduct == id_product)
                {
                    return Response;
                }
            }

            await Task.Run(() =>
            {
                var model = new Model.Wishlist.WishlistData
                {
                    id_wishlist = id_wishlist,
                    id_product = id_product,
                    username = username
                };


                var json = JsonConvert.SerializeObject(model);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = client.PostAsync("http://20.127.56.222:8080/api/Wishlist", httpContent);
                var mystring = response.GetAwaiter().GetResult();

                if (response.Result.IsSuccessStatusCode)
                {
                    Response = true;
                }
            });
            return Response;
        }

        public async Task<bool> DeleteWishlistAsync(int id_wishlist)
        {
            bool Response = false;
            await Task.Run(() =>
            {
                HttpClientHandler handler = GetInsecureHandler();
                var client = new HttpClient(handler);


                var response = client.DeleteAsync("http://20.127.56.222:8080/api/Wishlist/" + id_wishlist);
                var mystring = response.GetAwaiter().GetResult();

                if (response.Result.IsSuccessStatusCode)
                {
                    Response = true;
                }
            });
            return Response;
        }
        public static HttpClientHandler GetInsecureHandler()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
            {
                if (cert.Issuer.Equals("CN=localhost"))
                    return true;
                return errors == System.Net.Security.SslPolicyErrors.None;
            };
            return handler;
        }
    }
}
