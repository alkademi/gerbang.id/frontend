﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Gerbang.Views
{
    public partial class Home : ContentPage
    {
        public Home()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            Routing.RegisterRoute(nameof(Profile), typeof(Profile));
        }
    }
}
