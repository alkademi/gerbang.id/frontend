﻿using System;
using System.Collections.Generic;
using Gerbang.Services;
using Xamarin.Forms;


namespace Gerbang.Views
{
    public partial class Register : ContentPage
    {
        public Register()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            //procces_img.Isrunngin = true;
            if (((string.IsNullOrWhiteSpace(nama.Text))) ||
                ((string.IsNullOrWhiteSpace(ttl.Text))) ||
                ((string.IsNullOrWhiteSpace(email.Text))) ||
                ((string.IsNullOrWhiteSpace(uname.Text))) ||
                ((string.IsNullOrWhiteSpace(pw.Text))) ||
                ((string.IsNullOrWhiteSpace(pw2.Text))))
            {
                await DisplayAlert("Masukan Data !", "Semua Data Wajib Diisi !", "Ok");
            }
            else
            {
                try
                {
                    bool response = await ApiServices.ServiceClientInstance.RegisterUserAsync(nama.Text, ttl.Text, email.Text, uname.Text, pw.Text, pw2.Text);
                    if (response)
                    {
                        await DisplayAlert("Selamat", "Registrasi Berhasil !", "Ok");
                        await Navigation.PushAsync(new Login());
                    }
                    else
                    {
                        await DisplayAlert("Eror", "Registrasi Gagal !", "Ok");
                    }
                }
                catch (InvalidCastException)
                {
                    await DisplayAlert("Eror", "Registrasi Gagal !", "Ok");
                }
            }


        }

        private async void TGR_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Login());
        }
    }
}