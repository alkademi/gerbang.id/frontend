﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Gerbang.ViewModels;
using Newtonsoft.Json;
using System.Net.Http;
namespace Gerbang.Views
{
    public partial class BookPage : ContentPage
    {
        public BookPage()
        {
            InitializeComponent();
            ProductViewModel pvm = new ProductViewModel();
            pvm.Tab = 0;
            pvm.Type = 4;
            /*Console.WriteLine(pvm.Products[1].ProductSize);*/
            BindingContext = pvm;
        }
        protected override async void OnAppearing()
        {
            untukAndaBtn.TextColor = Color.FromHex("2058DC");
            untukAndaBox.Color = Color.FromHex("2058DC");

            anakAnakBtn.TextColor = Color.Silver;
            anakAnakBox.Color = Color.Silver;

            palingPopBtn.TextColor = Color.Silver;
            palingPopBox.Color = Color.Silver;
            HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
            HttpClient cli = new HttpClient(handler);
            ProductViewModel pvm = new ProductViewModel();
            BindingContext = pvm;
            pvm.Tab = 0;
            pvm.Type = 4;
            var url = "http://20.127.56.222:8080/api/Product?type=4&tab=0";
            var content = await cli.GetStringAsync(url);
            var ProductsWithoutId = new ObservableCollection<ProductItemViewModel>(JsonConvert.DeserializeObject<List<ProductItemViewModel>>(content));
            var Products = new ObservableCollection<ProductItemViewModelWithId>();
            int i = 1;
            foreach (ProductItemViewModel item in ProductsWithoutId)
            {
                Products.Add(new ProductItemViewModelWithId(i, item));
                i++;
            }

            pvm.Products = Products;

            var url2 = "http://20.127.56.222:8080/api/Category/4";
            var content2 = await cli.GetStringAsync(url2);
            var Categories = JsonConvert.DeserializeObject<IEnumerable<CategoryViewModel>>(content2) as List<CategoryViewModel>;
            pvm.Categories = Categories;
            pvm.IsLoading = false;
            pvm.IsLoaded = true;
            if (Products.Count <= 0)
            {
                pvm.IsNotFound = true;
            }
            else
            {
                pvm.IsNotFound = false;
            }
            BindingContext = pvm;
        }
        protected override void OnDisappearing()
        {
            categoryPicker.SelectedIndex = -1;
            sortPicker.SelectedIndex = -1;
            base.OnDisappearing();
        }

        private async void CreateButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CreateItemPage(0, -1));
        }

        private async void UntukButton_Clicked(Object sender, EventArgs e)
        {
            untukAndaBtn.TextColor = Color.FromHex("2058DC");
            untukAndaBox.Color = Color.FromHex("2058DC");

            anakAnakBtn.TextColor = Color.Silver;
            anakAnakBox.Color = Color.Silver;

            palingPopBtn.TextColor = Color.Silver;
            palingPopBox.Color = Color.Silver;
            HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
            HttpClient cli = new HttpClient(handler);
            ProductViewModel pvm = new ProductViewModel();
            pvm.Tab = 0;
            pvm.Type = 4;
            BindingContext = pvm;
            var url = "http://20.127.56.222:8080/api/Product?type=4&tab=0";
            var content = await cli.GetStringAsync(url);
            var ProductsWithoutId = new ObservableCollection<ProductItemViewModel>(JsonConvert.DeserializeObject<List<ProductItemViewModel>>(content));
            var Products = new ObservableCollection<ProductItemViewModelWithId>();
            int i = 1;
            foreach (ProductItemViewModel item in ProductsWithoutId)
            {
                Products.Add(new ProductItemViewModelWithId(i, item));
                i++;
            }

            pvm.Products = Products;

            var url2 = "http://20.127.56.222:8080/api/Category/4";
            var content2 = await cli.GetStringAsync(url2);
            var Categories = JsonConvert.DeserializeObject<IEnumerable<CategoryViewModel>>(content2) as List<CategoryViewModel>;
            pvm.Categories = Categories;
            pvm.IsLoading = false;
            pvm.IsLoaded = true;
            if (Products.Count <= 0)
            {
                pvm.IsNotFound = true;
            }
            else
            {
                pvm.IsNotFound = false;
            }

            BindingContext = pvm;
        }

        private async void PopulerButton_Clicked(Object sender, EventArgs e)
        {
            untukAndaBtn.TextColor = Color.Silver;
            untukAndaBox.Color = Color.Silver;

            anakAnakBtn.TextColor = Color.Silver;
            anakAnakBox.Color = Color.Silver;

            palingPopBtn.TextColor = Color.FromHex("2058DC");
            palingPopBox.Color = Color.FromHex("2058DC");
            HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
            HttpClient cli = new HttpClient(handler);
            ProductViewModel pvm = new ProductViewModel();
            pvm.Tab = 1;
            pvm.Type = 4;
            BindingContext = pvm;
            var url = "http://20.127.56.222:8080/api/Product?type=4&tab=1";
            var content = await cli.GetStringAsync(url);
            var ProductsWithoutId = new ObservableCollection<ProductItemViewModel>(JsonConvert.DeserializeObject<List<ProductItemViewModel>>(content));
            var Products = new ObservableCollection<ProductItemViewModelWithId>();
            int i = 1;
            foreach (ProductItemViewModel item in ProductsWithoutId)
            {
                Products.Add(new ProductItemViewModelWithId(i, item));
                i++;
            }

            pvm.Products = Products;

            var url2 = "http://20.127.56.222:8080/api/Category/1";
            var content2 = await cli.GetStringAsync(url2);
            var Categories = JsonConvert.DeserializeObject<IEnumerable<CategoryViewModel>>(content2) as List<CategoryViewModel>;
            pvm.Categories = Categories;
            pvm.IsLoading = false;
            pvm.IsLoaded = true;
            if (Products.Count <= 0)
            {
                pvm.IsNotFound = true;
            }
            else
            {
                pvm.IsNotFound = false;
            }

            BindingContext = pvm;
        }

        private async void AnakButton_Clicked(object sender, EventArgs e)
        {
            anakAnakBtn.TextColor = Color.FromHex("2058DC");
            anakAnakBox.Color = Color.FromHex("2058DC");

            untukAndaBtn.TextColor = Color.Silver;
            untukAndaBox.Color = Color.Silver;

            palingPopBtn.TextColor = Color.Silver;
            palingPopBox.Color = Color.Silver;
            HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
            HttpClient cli = new HttpClient(handler);
            ProductViewModel pvm = new ProductViewModel();
            pvm.Tab = 2;
            pvm.Type = 4;
            BindingContext = pvm;
            var url = "http://20.127.56.222:8080/api/Product?type=4&tab=2";
            var content = await cli.GetStringAsync(url);
            var ProductsWithoutId = new ObservableCollection<ProductItemViewModel>(JsonConvert.DeserializeObject<List<ProductItemViewModel>>(content));
            var Products = new ObservableCollection<ProductItemViewModelWithId>();
            int i = 1;
            foreach (ProductItemViewModel item in ProductsWithoutId)
            {
                Products.Add(new ProductItemViewModelWithId(i, item));
                i++;
            }

            pvm.Products = Products;

            var url2 = "http://20.127.56.222:8080/api/Category/4";
            var content2 = await cli.GetStringAsync(url2);
            var Categories = JsonConvert.DeserializeObject<IEnumerable<CategoryViewModel>>(content2) as List<CategoryViewModel>;
            pvm.Categories = Categories;
            pvm.IsLoading = false;
            pvm.IsLoaded = true;
            if (Products.Count <= 0)
            {
                pvm.IsNotFound = true;
            }
            else
            {
                pvm.IsNotFound = false;
            }

            BindingContext = pvm;
        }

        private async void SearchBar_Pressed(object sender, EventArgs e)
        {
            var keyword = AppSearchBar.Text;
            AppSearchBar.Text = "";
            await Navigation.PushAsync(new SearchPage(keyword, 4));
        }
    }
}