﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Gerbang.ViewModels;
using Newtonsoft.Json;
using System.Net.Http;

namespace Gerbang.Views
{
    public partial class SearchPage : ContentPage
    {
        int type;
        public SearchPage(String keyword,int t)
        {
            InitializeComponent();
            type = t;
            displayComponent(keyword);
        }

        public async void displayComponent(String keyword)
        {
            AppSearchBar.Text = keyword;
            HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
            HttpClient cli = new HttpClient(handler);
            ProductViewModel pvm = new ProductViewModel();
            BindingContext = pvm;
            var url = "http://20.127.56.222:8080/api/Product?name=" + keyword;
            var content = await cli.GetStringAsync(url);
            var ProductsWithoutId = new ObservableCollection<ProductItemViewModel>(JsonConvert.DeserializeObject<List<ProductItemViewModel>>(content));
            var Products = new ObservableCollection<ProductItemViewModelWithId>();
            int i = 1;
            foreach (ProductItemViewModel item in ProductsWithoutId)
            {
                if (type == 1 || type == 2)
                {
                    if (item.ProductTypesId == 1 || item.ProductTypesId == 2)
                    {
                        Products.Add(new ProductItemViewModelWithId(i, item));
                        i++;
                    }
                }
                else
                {
                    if(item.ProductTypesId == type)
                    {
                        Products.Add(new ProductItemViewModelWithId(i, item));
                        i++;
                    }
                }
            }
            if (Products.Count <= 0)
            {
                pvm.IsNotFound = true;
                NotFoundLabel.Text = "Tidak ditemukan hasil untuk " + keyword;
            }
            else
            {
                pvm.IsNotFound = false;
            }
            pvm.Products = Products;
            pvm.IsLoading = false;
            pvm.IsLoaded = true;
            BindingContext = pvm;
        }
        private async void SearchBar_Pressed(object sender, EventArgs e)
        {
            var keyword = AppSearchBar.Text;
            HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
            HttpClient cli = new HttpClient(handler);
            ProductViewModel pvm = new ProductViewModel();
            BindingContext = pvm;
            var url = "http://20.127.56.222:8080/api/Product?name=" + keyword;
            var content = await cli.GetStringAsync(url);
            var ProductsWithoutId = new ObservableCollection<ProductItemViewModel>(JsonConvert.DeserializeObject<List<ProductItemViewModel>>(content));
            var Products = new ObservableCollection<ProductItemViewModelWithId>();
            int i = 1;
            foreach (ProductItemViewModel item in ProductsWithoutId)
            {
                if (type == 1 || type == 2)
                {
                    if (item.ProductTypesId == 1 || item.ProductTypesId == 2)
                    {
                        Products.Add(new ProductItemViewModelWithId(i, item));
                        i++;
                    }
                }
                else
                {
                    if (item.ProductTypesId == type)
                    {
                        Products.Add(new ProductItemViewModelWithId(i, item));
                        i++;
                    }
                }
            }
            if (Products.Count <= 0)
            {
                pvm.IsNotFound = true;
                NotFoundLabel.Text = "Tidak ditemukan hasil untuk " + keyword;
            }
            else
            {
                pvm.IsNotFound = false;
            }
            pvm.Products = Products;
            pvm.IsLoading = false;
            pvm.IsLoaded = true;
            BindingContext = pvm;
        }

    }
}