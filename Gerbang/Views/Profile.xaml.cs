﻿using Gerbang.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using Xamarin.CommunityToolkit.Extensions;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Gerbang.Views
{
    public partial class Profile : ContentPage
    {
        private HttpClient client = new HttpClient(Services.ApiServices.GetInsecureHandler());      

        public Profile()
        {

            InitializeComponent();
            Shell.SetNavBarHasShadow(this, false);
            Shell.SetBackgroundColor(this, Color.FromHex("5A82EA"));
            Shell.SetForegroundColor(this, Color.FromHex("F7F7F7"));
            Shell.SetTitleColor(this, Color.FromHex("F7F7F7"));
            BindingContext = new ProfileViewModel();
        }

        protected override async void OnAppearing()
        {
            var username = await SecureStorage.GetAsync("username");
            var token = await SecureStorage.GetAsync("token");
            var Url = "http://20.127.56.222:8080/api/Users?user=" + username;
            AuthenticationHeaderValue authHeader = new AuthenticationHeaderValue("bearer", token);
            client.DefaultRequestHeaders.Authorization = authHeader;
            var content = await client.GetStringAsync(Url);
            BindingContext = JsonConvert.DeserializeObject<ProfileViewModel>(content);
            
            base.OnAppearing();
        }

        private async void OnWishlistClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new WishlistPage());
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            App.Current.Properties["IsLoggedIn"] = false;
            App.Current.MainPage = new NavigationPage(new Login());
        }
    }
}