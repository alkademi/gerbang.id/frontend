﻿using System;
using System.Collections.Generic;
using Gerbang.Helpers;
using Xamarin.Forms;
using Xamarin.Essentials;
using Gerbang.ViewModels;
using Gerbang.Model.Product;
using Gerbang.Services;

namespace Gerbang.Views
{
    public partial class CreateItemPage : ContentPage
    {
        public ItemViewModel vm { get; set; }
        public int FormType { get; set; }

        public CreateItemPage(int formType,int id)
        {
            InitializeComponent();
            Shell.SetBackgroundColor(this, Color.FromHex("5A82EA"));
            Shell.SetForegroundColor(this, Color.FromHex("F7F7F7"));
            Shell.SetTitleColor(this, Color.FromHex("F7F7F7"));
            Console.WriteLine("Pass");
            this.vm = BindingContext as ItemViewModel;
            Console.WriteLine("Pass2");
            this.vm.Id = id;

            FormType = formType;
            // Kondisi melihat apakah update data atau create data
            // 0 : Create data
            // 1 : Update data
            if (FormType == 0)
            {
                submit_button.Text = "Tambah";
            }
            else if (FormType == 1)
            {
                submit_button.Text = "Simpan";
                vm.getProduct(id);
            }


        }

        public async void OnSubmitCommand(object sender, EventArgs args)
        {
            var string_type_of_question = FormType == 0? "ditambahkan" : "diperbaharui";
            var answer = await DisplayAlert("Apakah anda sudah yakin?", $"Data produk akan {string_type_of_question} ke dalam database", "Ya", "Tidak");

            if (answer) 
            {
                var result = await (FormType == 0? vm.postNewProduct(): vm.updateProduct());

                if (result)
                {
                    await DisplayAlert("Sukses!", $"Data produk berhasil {string_type_of_question} ke dalam database", "Ok");
                    await Shell.Current.GoToAsync("../..");

                } else
                {
                    await DisplayAlert("Oops!", $"Data produk gagal {string_type_of_question} ke dalam database", "Ok");
                    await Shell.Current.GoToAsync("../..");
                }
            }
        }
    }
}
