﻿using System;
using System.Collections.Generic;
using Gerbang.Services;
using Gerbang.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Net.Http;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace Gerbang.Views
{
    public partial class DetailPageAdmin : ContentPage
    {
        public int _Id { get; set; }
        public int _IdWL { get; set; }
        public int _LengthReview { get; set; }
       

        public DetailPageAdmin(ProductItemViewModelWithId SelectedItem)
        {
            InitializeComponent();
            Shell.SetTabBarIsVisible(this, false);

            itemname.Text = SelectedItem.ProductName;
            deskripsi.Text = SelectedItem.ProductDesc;
            _Id = SelectedItem.ProductId;
            addTowishlist.Text = "Tambahkan ke wishlist";
            image.Source = SelectedItem.LogoSrc;
            publisher.Text = SelectedItem.ProductPublisher;
            rating.Text = SelectedItem.ProductRating.ToString() + $"★";
            ratingg.Text = SelectedItem.ProductRating.ToString();
            
            size.Text = SelectedItem.ProductSize.ToString() + " " + SelectedItem.ProductSizeUnit.ToString().ToUpper();
            if (String.Equals(SelectedItem.ProductAgeRestriction.ToString(), "child"))
            {
                age_icon.Source = "anak_icon.png";
                age.Text = "Anak-Anak";
            } else if (String.Equals(SelectedItem.ProductAgeRestriction.ToString(), "teen"))
            {
                age_icon.Source = "remaja_icon.png";
                age.Text = "Remaja";
            } else if (String.Equals(SelectedItem.ProductAgeRestriction.ToString(), "adult"))
            {
                age_icon.Source = "dewasa_icon.png";
                age.Text = "Dewasa";
            }
            for (int i = 0; i < SelectedItem.ImageSrc.Length; i++)
            {
                if (SelectedItem.ImageSrc.Length == 1)
                {
                    foto1.HeightRequest = 100;
                    foto1.WidthRequest = 56;
                    foto2.HeightRequest = 100;
                    foto2.WidthRequest = 56;
                    foto3.HeightRequest = 100;
                    foto3.WidthRequest = 56;
                    foto2.Source = SelectedItem.ImageSrc[i];
                }
                else if (SelectedItem.ImageSrc.Length == 2)
                {
                    foto1.HeightRequest = 100;
                    foto1.WidthRequest = 56;
                    foto2.HeightRequest = 100;
                    foto2.WidthRequest = 56;
                    foto3.HeightRequest = 100;
                    foto3.WidthRequest = 56;
                    if (i == 0)
                    {
                        foto1.Source = SelectedItem.ImageSrc[i];
                    }
                    else
                    {
                        foto3.Source = SelectedItem.ImageSrc[i];
                    }

                }
                else
                {
                    foto1.HeightRequest = 100;
                    foto1.WidthRequest = 56;
                    foto2.HeightRequest = 100;
                    foto2.WidthRequest = 56;
                    foto3.HeightRequest = 100;
                    foto3.WidthRequest = 56;
                    if (i == 0)
                    {
                        foto1.Source = SelectedItem.ImageSrc[i];
                    }
                    else if (i == 1)
                    {
                        foto2.Source = SelectedItem.ImageSrc[i];
                    }
                    else
                    {
                        foto3.Source = SelectedItem.ImageSrc[i];
                    }
                }
            }
            GetReview(_Id);

        }
        
        public DetailPageAdmin(ProductItemViewModelWithId SelectedItem, int isWL)
        {
            InitializeComponent();
            itemname.Text = SelectedItem.ProductName;
            deskripsi.Text = SelectedItem.ProductDesc;
            _Id = SelectedItem.ProductId;
            _IdWL = SelectedItem.Index;
            addTowishlist.Text = "Hapus dari wishlist";
            image.Source = SelectedItem.LogoSrc;
            publisher.Text = SelectedItem.ProductPublisher;
            rating.Text = SelectedItem.ProductRating.ToString() + $"★";
            ratingg.Text = SelectedItem.ProductRating.ToString();

            size.Text = SelectedItem.ProductSize.ToString() + " " + SelectedItem.ProductSizeUnit.ToString().ToUpper();

            if (String.Equals(SelectedItem.ProductAgeRestriction.ToString(), "child"))
            {
                age_icon.Source = "anak_icon.png";
                age.Text = "Anak-Anak";
            }
            else if (String.Equals(SelectedItem.ProductAgeRestriction.ToString(), "teen"))
            {
                age_icon.Source = "remaja_icon.png";
                age.Text = "Remaja";
            }
            else if (String.Equals(SelectedItem.ProductAgeRestriction.ToString(), "adult"))
            {
                age_icon.Source = "dewasa_icon.png";
                age.Text = "Dewasa";
            }
            for (int i = 0; i < SelectedItem.ImageSrc.Length; i++)
            {
                if (SelectedItem.ImageSrc.Length == 1)
                {
                    foto2.Source = SelectedItem.ImageSrc[i];
                }
                else if (SelectedItem.ImageSrc.Length == 2)
                {
                    if (i == 0)
                    {
                        foto1.Source = SelectedItem.ImageSrc[i];
                    }
                    else
                    {
                        foto3.Source = SelectedItem.ImageSrc[i];
                    }

                }
                else
                {
                    if (i == 0)
                    {
                        foto1.Source = SelectedItem.ImageSrc[i];
                    }
                    else if (i == 1)
                    {
                        foto2.Source = SelectedItem.ImageSrc[i];
                    }
                    else
                    {
                        foto3.Source = SelectedItem.ImageSrc[i];
                    }
                }
            }
            GetReview(_Id);
        }
        private async void GetReview(int id)
        {
            HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
            HttpClient cli = new HttpClient(handler);
            ReviewViewModel rvm = new ReviewViewModel();
            BindingContext = rvm;
            var url = "http://20.127.56.222:8080/api/ProductReviews?product_id=" + id;
            var content = await cli.GetStringAsync(url);
            var reviews = new ObservableCollection<ReviewItemViewModel>(JsonConvert.DeserializeObject<List<ReviewItemViewModel>>(content));
            Console.WriteLine(content);
            Console.WriteLine(reviews);
            _LengthReview = reviews.Count;
            if (_LengthReview > 0)
            {
                rvm.IsUlasan = false;
            }
            else
            {
                rvm.IsUlasan = true;
            }
            Console.WriteLine(_LengthReview);
            rvm.Reviews = reviews;
            ulasan.Text = _LengthReview.ToString() + " " + "Ulasan";

        }
        


        private async void Toolbar_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CreateItemPage(1,_Id));
        }

        private async void Delete_Clicked(object sender, EventArgs e)
        {
            bool Msg = await DisplayAlert("Perhatian !", "Apakah Anda Yakin ingin Menghapus ?", "Yes", "No");
            if (Msg == true)
            {
                try
                {
                    var iddelete = _Id;
                    bool response = await ApiServices.ServiceClientInstance.DeleteCatalogAsync(iddelete);
                    if (response)
                    {
                        await DisplayAlert("Selamat", "Data berhasil dihapus", "Ok");
                        await Shell.Current.GoToAsync("..");
                    }
                    else
                    {
                        await DisplayAlert("Eror", "Data gagal dihapus", "Ok");
                    }
                }
                catch (InvalidCastException)
                {
                    await DisplayAlert("Eror", "Data gagal dihapus", "Ok");
                }
            }
            else
            {
                await DisplayAlert("Perhatian", "Hati-hati", "Ok");
            }
        }
        private async void WishlistButton(object sender, EventArgs e)
        {
            if (addTowishlist.Text == "Tambahkan ke wishlist") { 
                bool Msg = await DisplayAlert("Perhatian !", "Tambah produk ke dalam Wishlist?", "Yes", "No");
                if (Msg)
                {
                    try
                    {
                        var id_prod= _Id;
                        var username = await SecureStorage.GetAsync("username");
                        bool response = await ApiServices.ServiceClientInstance.AddWishlistAsync(id_prod, username);
                        if (response)
                        {
                            await DisplayAlert("Selamat", "Produk berhasil ditambah ke wishlist", "Ok");
                            await Shell.Current.GoToAsync("..");
                        }
                        else
                        {
                            await DisplayAlert("Eror", "Produk gagal ditambah ke wishlist", "Ok");
                        }
                    }
                    catch (InvalidCastException)
                    {
                        await DisplayAlert("Eror", "Produk gagal ditambah ke wishlist", "Ok");
                    }
                }
                else
                {
                    await DisplayAlert("Perhatian", "Hati-hati", "Ok");
                }
            }
            else if(addTowishlist.Text == "Hapus dari wishlist")
            {
                bool Msg = await DisplayAlert("Perhatian !", "Hapus produk dari Wishlist?", "Yes", "No");
                if (Msg)
                {
                    try
                    {
                        var idx = _IdWL;
                        bool response = await ApiServices.ServiceClientInstance.DeleteWishlistAsync(idx);
                        if (response)
                        {
                            await DisplayAlert("Selamat", "Produk berhasil dihapus dari wishlist", "Ok");
                            await Shell.Current.GoToAsync("..");
                        }
                        else
                        {
                            await DisplayAlert("Eror", "Produk gagal dihapus dari wishlist", "Ok");
                        }
                    }
                    catch (InvalidCastException)
                    {
                        await DisplayAlert("Eror", "Produk gagal dihapus dari wishlist", "Ok");
                    }
                }
                else
                {
                    await DisplayAlert("Perhatian", "Hati-hati", "Ok");
                }
            }
        }
    }
}
