﻿using Gerbang.Models;
using Gerbang.ViewModels;
using Gerbang.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gerbang.Views
{
    public partial class WishlistPage : ContentPage
    {
        public WishlistPage()
        {
            InitializeComponent();
            Shell.SetBackgroundColor(this, Color.FromHex("5A82EA"));
            Shell.SetForegroundColor(this, Color.FromHex("F7F7F7"));
            Shell.SetTitleColor(this, Color.FromHex("F7F7F7"));
            ProductViewModel pvm = new ProductViewModel();
            pvm.IsLoading = true;
            pvm.IsNotFound = false;
            pvm.IsLoaded = true;
            BindingContext = pvm;
        }

        protected override async void OnAppearing()
        {
            HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
            HttpClient cli = new HttpClient(handler);
            var username = await SecureStorage.GetAsync("username");
            var url0 = "http://20.127.56.222:8080/api/Wishlist";
            var content0 = await cli.GetStringAsync(url0);
            var Wishlist = new ObservableCollection<WishlistViewModel>(JsonConvert.DeserializeObject<List<WishlistViewModel>>(content0));
            var WLUser = new ObservableCollection<WishlistViewModel>();
            foreach (WishlistViewModel itemWL in Wishlist)
            {
                if (itemWL.Username == username)
                {
                    WLUser.Add(itemWL);
                }
            }

            ProductViewModel pvm = new ProductViewModel();
            BindingContext = pvm;
            var url1 = "http://20.127.56.222:8080/api/Product";
            var content1 = await cli.GetStringAsync(url1);
            var ProductsWithoutId = new ObservableCollection<ProductItemViewModel>(JsonConvert.DeserializeObject<List<ProductItemViewModel>>(content1));
            var Products = new ObservableCollection<ProductItemViewModelWithId>();
            foreach (ProductItemViewModel item in ProductsWithoutId)
            {
                foreach (WishlistViewModel itemWL in WLUser)
                {
                    if (item.ProductId == itemWL.IdProduct)
                    {
                        Products.Add(new ProductItemViewModelWithId(itemWL.IdWishlist, item));
                    }
                }
            }
            
            pvm.Products = Products;
            pvm.IsLoading = false;
            pvm.IsLoaded = true;
            if (Products.Count <= 0)
            {
                pvm.IsNotFound = true;
            }
            else
            {
                pvm.IsNotFound = false;
            }
            BindingContext = pvm;

            base.OnAppearing();
        }
    }
}