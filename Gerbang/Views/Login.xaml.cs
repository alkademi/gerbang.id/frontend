﻿using System;
using System.Collections.Generic;
using Gerbang.Model.Login;
using Gerbang.Services;
using Xamarin.Forms;
using Xamarin.Essentials;
using Xamarin.Auth;
using Newtonsoft.Json;
using Gerbang.Helpers;
using System.Linq;
using Gerbang.Model.Profile;
using System.Net.Http;
using System.Collections.ObjectModel;

namespace Gerbang.Views
{
    public partial class Login : ContentPage
    {
        Account account;
        AccountStore store;
        public Login()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            store = AccountStore.Create();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            /*await Shell.Current.GoToAsync("//Home");*/
            var content = await ApiServices.ServiceClientInstance.AuthenticateUserAsync(txtUsername.Text, txtPassword.Text);
            if (!Object.ReferenceEquals(content, null))
            {
                App.Current.Properties["IsLoggedIn"] = true;
                var role = await ApiServices.ServiceClientInstance.GetUserRole(content.username);
                Console.WriteLine(content.username);
                Console.WriteLine(role[0].username);
                Console.WriteLine(role[0].roleid);
                if (role[0].roleid == 2) //Admin page
                {
                    Console.WriteLine("Masuk ke admin");
                    App.Current.Properties["Role"] = "Admin";
                    Application.Current.MainPage = new AppShell();
                }
                else //User page
                {
                    Console.WriteLine("Masuk ke user");
                    App.Current.Properties["Role"] = "User";
                    Application.Current.MainPage = new AppShellUser();
                }

            }
            else
            {
                await DisplayAlert("Ooops :(", "Username atau Password salah!", "Ok");
            }
        }


        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Register());
        }

        void Google_Clicked(object sender, EventArgs e)
        {
            string clientId = "1051861326142-jre1bi2b54i3tv88ckv82k1les2ibdoi.apps.googleusercontent.com";
            string redirectUri = "com.googleusercontent.apps.1051861326142-jre1bi2b54i3tv88ckv82k1les2ibdoi:/oauth2redirect";
            string appName = "Gerbang";
            string scope = "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile";
            string authorizedUrl = "https://accounts.google.com/o/oauth2/auth";
            string accessTokenUrl = "https://www.googleapis.com/oauth2/v4/token";
            account = store.FindAccountsForService(appName).FirstOrDefault();
            Uri auth = new Uri(authorizedUrl);
            Uri red = new Uri(redirectUri);
            Uri accToken = new Uri(accessTokenUrl);
            Console.WriteLine("Calling auth");
            var authenticator = new OAuth2Authenticator(
                clientId,
                null,
                scope,
                auth,
                red,
                accToken,
                null,
                true);
            authenticator.Completed += OnAuthCompleted;
            authenticator.Error += OnAuthError;
           
            AuthenticationState.Authenticator = authenticator;
            var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            presenter.Login(authenticator);

        }
        async void OnAuthCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            
            Console.WriteLine("Onauthcompleted called");
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                Console.WriteLine("auth not null called");
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            GoogleUser user = null;

            if (e.IsAuthenticated)
            {
                // If the user is authenticated, request their basic user data from Google
                Console.WriteLine("IsAuth called");
                var UserInfoUrl = "https://www.googleapis.com/oauth2/v2/userinfo";
                var request = new OAuth2Request("GET", new Uri(UserInfoUrl), null, e.Account);
                var pss = "JZTQDb957t6ZXU6k";
                var response = await request.GetResponseAsync();
                if (response != null)
                {
                    string userJson = await response.GetResponseTextAsync();
                    Console.WriteLine(userJson);
                    user = JsonConvert.DeserializeObject<GoogleUser>(userJson);
                    

                }

                if (user != null)
                {
                    HttpClientHandler handler = Services.ApiServices.GetInsecureHandler();
                    HttpClient cli = new HttpClient(handler);
                    //HttpClient cli = new HttpClient();
                    //var url = "https://10.0.2.2:44313/api/Users/username?email=" + user.Email;
                    var url = "http://20.127.56.222:8080/api/Users/username?email=" + user.Email;
                    var ctt = await cli.GetStringAsync(url);
                    var uname = JsonConvert.DeserializeObject<List<GoogleUser>>(ctt);
                    if(uname.Count > 0)
                    {
                        Console.WriteLine("Uname :");
                        Console.WriteLine(uname[0].Name);
                        var apicontent = await ApiServices.ServiceClientInstance.AuthenticateUserAsync(uname[0].Name, pss);
                        if (!Object.ReferenceEquals(apicontent, null))
                        {
                            App.Current.Properties["Role"] = "anyone";
                            Application.Current.MainPage = new AppShellUser();
                        }
                    }
                    else
                    {
                        Console.WriteLine("null name");
                        try
                        {
                            char atSign = '@';
                            string unem = user.Email.Split(atSign)[0];
                            bool resp = await ApiServices.ServiceClientInstance.RegisterUserAsync(user.Name, "2001-04-30T08:11:33.586Z", user.Email,unem,pss, pss);
                            if (resp)
                            {
                                var apicontent = await ApiServices.ServiceClientInstance.AuthenticateUserAsync(user.Email, pss);
                                if (!Object.ReferenceEquals(apicontent, null))
                                {
                                    App.Current.Properties["Role"] = "anyone";
                                    Application.Current.MainPage = new AppShellUser();
                                }

                            }
                            else
                            {
                                await DisplayAlert("Eror", "Registrasi Gagal !", "Ok");
                            }
                        }
                        catch (InvalidCastException)
                        {
                            await DisplayAlert("Eror", "Registrasi Gagal !", "Ok");
                        }
                    }
                   

                }
                }
                //Application.Current.MainPage = new AppShellUser();
            }

            void OnAuthError(object sender, AuthenticatorErrorEventArgs e)
            {
                Console.WriteLine("OnAuthError called");
                var authenticator = sender as OAuth2Authenticator;
                if (authenticator != null)
                {
                    authenticator.Completed -= OnAuthCompleted;
                    authenticator.Error -= OnAuthError;
                }

                Console.WriteLine("Authentication error: " + e.Message);
            }

    }
}
