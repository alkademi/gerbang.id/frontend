﻿using System;
using System.Threading.Tasks;
using Gerbang.Services;
using Gerbang.Views;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: ExportFont("Montserrat-Bold.ttf", Alias = "Montserrat-Bold")]
[assembly: ExportFont("Montserrat-ExtraBold.ttf", Alias = "Montserrat-ExtraBold")]
[assembly: ExportFont("Montserrat-Medium.ttf", Alias = "Montserrat-Medium")]
[assembly: ExportFont("Montserrat-Regular.ttf", Alias = "Montserrat-Regular")]
[assembly: ExportFont("Montserrat-SemiBold.ttf", Alias = "Montserrat-SemiBold")]

namespace Gerbang
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            //MainPage = new AppShell();
            var isLoggedIn = Properties.ContainsKey("IsLoggedIn") ? (bool)Properties["IsLoggedIn"] : false;
            if (isLoggedIn)
            {
                Console.WriteLine("Checkpoint Logged In");
                var uname = GetUsername();
                if (String.Equals(uname.ToString(), "busted"))
                {
                    Console.WriteLine("masuk if");
                    MainPage = new AppShell();
                }
                else
                {
                    Console.WriteLine("masuk else");
                    MainPage = new AppShellUser();
                }

            }
            else
            {
                Console.WriteLine("Checkpoint Not yet Logged In");
                MainPage = new NavigationPage(new Login());
            }
            
            // dependecy injection for product response
            DependencyService.Register<IProduct, Product>();

            //MainPage = new NavigationPage(new Login());

        }

        protected override async void OnStart()
        {
            
        }

        //private async Task<bool> IsLoggedIn()
        //{
        //    bool login;
        //    Console.WriteLine("Checking User Storage");
        //    var key = GetUsername();
        //    Console.WriteLine(key.ToString());
        //    if (Task.FromResult(key.NullIfEmpty()) != null ))
        //    {
        //        login = false;
                
        //    }
        //    else
        //    {
        //        login = true;
        //    }
        //    Console.WriteLine(login);
        //    return login;
        //}

        private async Task<string> GetUsername()
        {
            string key = await SecureStorage.GetAsync("username");
            return key;
        }
        
        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
