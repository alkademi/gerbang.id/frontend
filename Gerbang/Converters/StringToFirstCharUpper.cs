﻿using System;
using Xamarin.Forms;

namespace Gerbang.Converters
{

    public class StringToFirstCharUpper : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return string.Empty;

            string firstChar = value.ToString().Substring(0, 1).ToUpper();

            return firstChar;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
