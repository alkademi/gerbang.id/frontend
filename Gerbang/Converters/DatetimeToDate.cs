﻿using System;
using Xamarin.Forms;

namespace Gerbang.Converters
{
    public class DatetimeToDate : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return string.Empty;
            
            string d = value.ToString().Substring(8, 2);
            string m = value.ToString().Substring(5, 2);
            string m_convert;
            switch (m)
            {
                case "01":
                    m_convert = "Januari"; break;
                case "02":
                    m_convert = "Februari"; break;
                case "03":
                    m_convert = "Maret"; break;
                case "04":
                    m_convert = "April"; break;
                case "05":
                    m_convert = "Mei"; break;
                case "06":
                    m_convert = "Juni"; break;
                case "07":
                    m_convert = "Juli"; break;
                case "08":
                    m_convert = "Agustus"; break;
                case "09":
                    m_convert = "September"; break;
                case "10":
                    m_convert = "Oktober"; break;
                case "11":
                    m_convert = "November"; break;
                default:
                    m_convert = "Desember"; break;
            }

            string y = value.ToString().Substring(0, 4);
            string date = d + " " + m_convert + " " + y;
            return date;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
