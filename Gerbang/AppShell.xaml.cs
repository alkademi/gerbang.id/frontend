﻿using Gerbang.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Gerbang
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(Home), typeof(Home));
            Routing.RegisterRoute(nameof(CreateItemPage), typeof(CreateItemPage));
            Routing.RegisterRoute(nameof(AppPage), typeof(AppPage));
            Routing.RegisterRoute(nameof(BookPage), typeof(BookPage));
            Routing.RegisterRoute(nameof(FilmPage), typeof(FilmPage));
            Routing.RegisterRoute(nameof(GamePage), typeof(GamePage));
            
        }
    }
}
