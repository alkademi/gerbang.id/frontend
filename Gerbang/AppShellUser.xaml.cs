﻿using Gerbang.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Gerbang
{
    public partial class AppShellUser : Xamarin.Forms.Shell
    {
        public AppShellUser()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(Home), typeof(Home));
            Routing.RegisterRoute(nameof(AppPageUser), typeof(AppPageUser));
            Routing.RegisterRoute(nameof(BookPageUser), typeof(BookPageUser));
            Routing.RegisterRoute(nameof(FilmPageUser), typeof(FilmPageUser));
            Routing.RegisterRoute(nameof(GamePageUser), typeof(GamePageUser));
        }
    }
}
