﻿using System;
namespace Gerbang.Model.Register
{
    public class RegisterBindingModel
    {
        public string fullName { get; set; }
        public string dateOfBirth { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string passwordConfirmation { get; set; }
    }
}
