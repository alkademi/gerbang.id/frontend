﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gerbang.Models.Profile
{
    internal class ProfileData
    {
        public string Name { get; set; }
        public string DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
    }
}
