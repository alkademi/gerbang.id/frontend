﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gerbang.Model.Wishlist
{
    public class WishlistData
    {
        public int id_wishlist { get; set; }
        public int id_product { get; set; }
        public string username { get; set; }
    }
}
