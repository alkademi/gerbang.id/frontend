﻿using System;
using System.Collections.Generic;

namespace Gerbang.Model.Product
{
    public class ProductModel
    {
        public int productId { get; set; }
        public string productName { get; set; }
        public int productTypesId { get; set; }
        public int productCategoryId { get; set; }
        public string productPublisher { get; set; }
        public float productRating { get; set; }
        public float productSize { get; set; }
        public string productSizeUnit { get; set; }
        public string productAgeRestriction { get; set; }
        public string productDesc { get; set; }
        public string productLogo { get; set; }
        public string productLogoName { get; set; }
        public List<string> productImages { get; set; }
        public List<string> productImagesName { get; set; }
    }
}
