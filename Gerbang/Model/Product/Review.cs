﻿using System;
namespace Gerbang.Model.Product
{
    public class Review
    {
        public int reviewId { get; set; }
        public int productId { get; set; }
        public int userId { get; set; }
        public int stars { get; set; }
        public string reviewDate { get; set; }
        public string reviewText { get; set; }
        public string userName { get; set; }
    }
}

   