﻿using System;
namespace Gerbang.Model.Product
{
    public class ProductTypesModel
    {
        public int ProductTypesId { get; set; }
        public string ProductTypesName { get; set; }
    }
}
