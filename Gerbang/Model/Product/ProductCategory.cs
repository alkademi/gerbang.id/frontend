﻿using System;
namespace Gerbang.Model.Product
{
    public class ProductCategory
    {
        public int categoryId { get; set; }
        public int productTypesId { get; set; }
        public string categoryName { get; set; }
    }
}
