﻿using System;
namespace Gerbang.Model.Login
{
    public class LoginApiResponseModel
    {
        public string username { get; set; }
        public string token { get; set; }
        public string refreshToken { get; set; }
    }
}
