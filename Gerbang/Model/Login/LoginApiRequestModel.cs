﻿using System;
namespace Gerbang.Model.Login
{
    public class LoginApiRequestModel
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
